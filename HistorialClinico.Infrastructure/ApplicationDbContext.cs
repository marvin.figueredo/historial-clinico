﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using HistorialClinico.Domain;

namespace HistorialClinico.Infrastructure
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Anticonvulsionante> Anticonvulsionante { get; set; }
        public virtual DbSet<ApCardiovascular> ApCardiovascular { get; set; }
        public virtual DbSet<ApCardiovascularEnzimasCardiacas> ApCardiovascularEnzimasCardiacas { get; set; }
        public virtual DbSet<ApCardiovascularInotropicos> ApCardiovascularInotropicos { get; set; }
        public virtual DbSet<ApRespiratorio> ApRespiratorio { get; set; }
        public virtual DbSet<ApRespiratorioParametros> ApRespiratorioParametros { get; set; }
        public virtual DbSet<ApRespiratorioParametrosGasometria> ApRespiratorioParametrosGasometria { get; set; }
        public virtual DbSet<AspectoGeneralSNC> AspectoGeneralSNC { get; set; }
        public virtual DbSet<BalanceHidricoHMN> BalanceHidricoHMN { get; set; }
        public virtual DbSet<Cama> Cama { get; set; }
        public virtual DbSet<CamaPaciente> CamaPaciente { get; set; }
        public virtual DbSet<CategoriaLaboratorio> CategoriaLaboratorio { get; set; }
        public virtual DbSet<Cirugia> Cirugia { get; set; }
        public virtual DbSet<CoberturaAntibioticos> CoberturaAntibioticos { get; set; }
        public virtual DbSet<ContactoPaciente> ContactoPaciente { get; set; }
        public virtual DbSet<Cultivo> Cultivo { get; set; }
        public virtual DbSet<CultivoInfectologico> CultivoInfectologico { get; set; }
        public virtual DbSet<Diagnostico> Diagnostico { get; set; }
        public virtual DbSet<EnzimasCardiacas> EnzimasCardiacas { get; set; }
        public virtual DbSet<EstadoInfectologico> EstadoInfectologico { get; set; }
        public virtual DbSet<Gasometria> Gasometria { get; set; }
        public virtual DbSet<GeneralHMN> GeneralHMN { get; set; }
        public virtual DbSet<Hematologico> Hematologico { get; set; }
        public virtual DbSet<Hisopado> Hisopado { get; set; }
        public virtual DbSet<HisopadoInfectologico> HisopadoInfectologico { get; set; }
        public virtual DbSet<HMN> HMN { get; set; }
        public virtual DbSet<HMNBalanceHidricoHMN> HMNbalanceHidricoHMN { get; set; }
        public virtual DbSet<HMNGeneralHMN> HMNgeneralHMN { get; set; }
        public virtual DbSet<HMNLaboratorioHMN> HMNlaboratorioHMN { get; set; }
        public virtual DbSet<ImagenSNC> ImagenSNC { get; set; }
        public virtual DbSet<Infectologico> Infectologico { get; set; }
        public virtual DbSet<Inotropico> Inotropicos { get; set; }
        public virtual DbSet<LaboratorioHMN> LaboratorioHMN { get; set; }
        public virtual DbSet<LaboratorioSNC> LaboratorioSNC { get; set; }
        public virtual DbSet<MedicacionSNC> MedicacionSNC { get; set; }
        public virtual DbSet<MedicamentoSedacion> MedicamentoSedacion { get; set; }
        public virtual DbSet<Modalidad> Modalidad { get; set; }
        public virtual DbSet<MovimientoCamaPaciente> MovimientoCamaPaciente { get; set; }
        public virtual DbSet<Paciente> Paciente { get; set; }
        public virtual DbSet<PacienteTablaHistorica> PacienteTablaHistorica { get; set; }
        public virtual DbSet<ParametrosGasometria> ParametrosGasometria { get; set; }
        public virtual DbSet<ParametrosSopRespiratorio> ParametrosSopRespiratorio { get; set; }
        public virtual DbSet<PRMS> PRMS { get; set; }
        public virtual DbSet<Sedacion> Sedacion { get; set; }
        public virtual DbSet<SensibilidadCultivoInfectologico> SensibilidadCultivoInfectologico { get; set; }
        public virtual DbSet<SNC> SNC { get; set; }
        public virtual DbSet<SNCanticonvulsionante> SNCanticonvulsionante { get; set; }
        public virtual DbSet<SNCaspectoGeneralSNC> SNCaspectoGeneralSNC { get; set; }
        public virtual DbSet<SNCimagenesSNC> SNCimagenesSNC { get; set; }
        public virtual DbSet<SNClaboratorioSNC> SNClaboratorioSNC { get; set; }
        public virtual DbSet<SNCmedicacionSNC> SNCmedicacionSNC { get; set; }
        public virtual DbSet<SNCmedicamentoSedacion> SNCmedicamentoSedacion { get; set; }
        public virtual DbSet<SNCsxAbstinenciaSNC> SNCsxAbstinenciaSNC { get; set; }
        public virtual DbSet<SoporteRespiratorio> SoporteRespiratorio { get; set; }
        public virtual DbSet<SxAbstinenciaSNC> SxAbstinenciaSNC { get; set; }
        public virtual DbSet<Ventilacion> Ventilacion { get; set; }
        public DbSet<Paciente> Pacientes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<Anticonvulsionante>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<ApCardiovascular>(entity =>
            {
                entity.Property(e => e.DateAdd).HasColumnType("datetime");

                entity.Property(e => e.DateDelete).HasColumnType("datetime");

                entity.Property(e => e.EstadoId)
                    .IsRequired()
                    .HasMaxLength(1);

                entity.Property(e => e.EvaluacionCardiologica).HasMaxLength(800);

                entity.Property(e => e.Eventos).HasMaxLength(800);

                entity.Property(e => e.Planes).HasMaxLength(800);

                entity.Property(e => e.UserAdd)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserDelete).HasMaxLength(50);
            });

            builder.Entity<ApCardiovascularEnzimasCardiacas>(entity =>
            {
                entity.HasKey(e => new { e.ApCardiovascularId, e.EnzimasCardiacasId })
                    .HasName("PK__ApCardio__CAA8F0753B57158A");

                entity.Property(e => e.Curva)
                    .IsRequired()
                    .HasMaxLength(1);

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.ApCardiovascular)
                    .WithMany(p => p.ApCardiovascularEnzimasCardiacas)
                    .HasForeignKey(d => d.ApCardiovascularId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ApCardiov__ApCar__6501FCD8");

                entity.HasOne(d => d.EnzimasCardiacas)
                    .WithMany(p => p.ApCardiovascularEnzimasCardiacas)
                    .HasForeignKey(d => d.EnzimasCardiacasId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ApCardiov__Enzim__65F62111");
            });

            builder.Entity<ApCardiovascularInotropicos>(entity =>
            {
                entity.HasKey(e => new { e.ApCardiovascularId, e.InotropicosId })
                    .HasName("PK__ApCardio__B2DCFF4091DCE8AE");

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.ApCardiovascular)
                    .WithMany(p => p.ApCardiovascularInotropicos)
                    .HasForeignKey(d => d.ApCardiovascularId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ApCardiov__ApCar__61316BF4");

                entity.HasOne(d => d.Inotropicos)
                    .WithMany(p => p.ApCardiovascularInotropicos)
                    .HasForeignKey(d => d.InotropicosId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ApCardiov__Inotr__6225902D");
            });

            builder.Entity<ApRespiratorio>(entity =>
            {
                entity.Property(e => e.DateAdd).HasColumnType("datetime");

                entity.Property(e => e.DateDelete).HasColumnType("datetime");

                entity.Property(e => e.Eventos).HasMaxLength(800);

                entity.Property(e => e.Manejo).HasMaxLength(800);

                entity.Property(e => e.Planes).HasMaxLength(800);

                entity.Property(e => e.UserAdd)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserDelete).HasMaxLength(50);

                entity.Property(e => e.ValorSoporteResp).HasMaxLength(80);

                entity.HasOne(d => d.Gasometria)
                    .WithMany(p => p.ApRespiratorio)
                    .HasForeignKey(d => d.GasometriaId)
                    .HasConstraintName("FK__ApRespira__Gasom__3DB3258D");

                entity.HasOne(d => d.Modalidad)
                    .WithMany(p => p.ApRespiratorio)
                    .HasForeignKey(d => d.ModalidadId)
                    .HasConstraintName("FK__ApRespira__Modal__3CBF0154");

                entity.HasOne(d => d.SoporteRespiratorio)
                    .WithMany(p => p.ApRespiratorio)
                    .HasForeignKey(d => d.SoporteRespiratorioId)
                    .HasConstraintName("FK__ApRespira__Sopor__3AD6B8E2");

                entity.HasOne(d => d.Ventilacion)
                    .WithMany(p => p.ApRespiratorio)
                    .HasForeignKey(d => d.VentilacionId)
                    .HasConstraintName("FK__ApRespira__Venti__3BCADD1B");
            });

            builder.Entity<ApRespiratorioParametros>(entity =>
            {
                entity.HasKey(e => new { e.ApRespiratorioId, e.ParametroId })
                    .HasName("PK__ApRespir__7BBA3629EC2A25E2");

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.ApRespiratorio)
                    .WithMany(p => p.ApRespiratorioParametros)
                    .HasForeignKey(d => d.ApRespiratorioId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ApRespira__ApRes__408F9238");

                entity.HasOne(d => d.Parametro)
                    .WithMany(p => p.ApRespiratorioParametros)
                    .HasForeignKey(d => d.ParametroId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ApRespira__Param__4183B671");
            });

            builder.Entity<ApRespiratorioParametrosGasometria>(entity =>
            {
                entity.HasKey(e => new { e.ApRespiratorioId, e.ParametroId })
                    .HasName("PK__ApRespir__7BBA3629441C71F0");

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.ApRespiratorio)
                    .WithMany(p => p.ApRespiratorioParametrosGasometria)
                    .HasForeignKey(d => d.ApRespiratorioId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ApRespira__ApRes__4460231C");

                entity.HasOne(d => d.Parametro)
                    .WithMany(p => p.ApRespiratorioParametrosGasometria)
                    .HasForeignKey(d => d.ParametroId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ApRespira__Param__45544755");
            });

            builder.Entity<AspectoGeneralSNC>(entity =>
            {
                entity.ToTable("AspectoGeneralSNC");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<BalanceHidricoHMN>(entity =>
            {
                entity.ToTable("BalanceHidricoHMN");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<Cama>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<CamaPaciente>(entity =>
            {
                entity.HasKey(e => new { e.CamaId, e.PacienteId })
                    .HasName("PK__CamaPaci__9F6B910C90775F76");

                entity.HasIndex(e => e.CamaId)
                    .HasName("UQ__CamaPaci__765EAD0C3E3A5D66")
                    .IsUnique();

                entity.HasIndex(e => e.PacienteId)
                    .HasName("UQ__CamaPaci__9353C01E74CE701A")
                    .IsUnique();

                entity.Property(e => e.FechaIngreso).HasColumnType("datetime");

                entity.HasOne(d => d.Cama)
                    .WithOne(p => p.CamaPaciente)
                    .HasForeignKey<CamaPaciente>(d => d.CamaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CamaPacie__CamaI__76619304");

                entity.HasOne(d => d.Paciente)
                    .WithOne(p => p.CamaPaciente)
                    .HasForeignKey<CamaPaciente>(d => d.PacienteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CamaPacie__Pacie__7755B73D");
            });

            builder.Entity<CategoriaLaboratorio>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<Cirugia>(entity =>
            {
                entity.Property(e => e.CualProcedimiento).HasMaxLength(800);

                entity.Property(e => e.DateAdd).HasColumnType("datetime");

                entity.Property(e => e.DateDelete).HasColumnType("datetime");

                entity.Property(e => e.Hallazgos).HasMaxLength(800);

                entity.Property(e => e.OtrasAcotaciones).HasMaxLength(800);

                entity.Property(e => e.Tecnica).HasMaxLength(800);

                entity.Property(e => e.UserAdd)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserDelete).HasMaxLength(50);
            });

            builder.Entity<CoberturaAntibioticos>(entity =>
            {
                entity.Property(e => e.Antibiotico)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Dosis).HasColumnType("numeric(10, 4)");

                entity.Property(e => e.FechaInicio).HasColumnType("datetime");

                entity.Property(e => e.FechaSuspension).HasColumnType("datetime");

                entity.Property(e => e.Unidad)
                    .IsRequired()
                    .HasMaxLength(80);

                entity.HasOne(d => d.Infectologico)
                    .WithMany(p => p.CoberturaAntibioticos)
                    .HasForeignKey(d => d.InfectologicoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Cobertura__Infec__731B1205");
            });

            builder.Entity<ContactoPaciente>(entity =>
            {
                entity.Property(e => e.FechaAdd).HasColumnType("datetime");

                entity.Property(e => e.FechaDelete).HasColumnType("datetime");

                entity.Property(e => e.FechaUpdate).HasColumnType("datetime");

                entity.Property(e => e.NombreContacto)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.NroContacto).HasMaxLength(50);

                entity.Property(e => e.TipoContactoId)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.UserNameAdd)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserNameDelete).HasMaxLength(50);

                entity.Property(e => e.UserNameUpdate).HasMaxLength(50);
            });

            builder.Entity<Cultivo>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<CultivoInfectologico>(entity =>
            {
                entity.HasKey(e => new { e.InfectologicoId, e.CultivoId })
                    .HasName("PK__CultivoI__897F900D9D51FCFA");

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.HasOne(d => d.Cultivo)
                    .WithMany(p => p.CultivoInfectologico)
                    .HasForeignKey(d => d.CultivoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CultivoIn__Culti__78D3EB5B");

                entity.HasOne(d => d.Infectologico)
                    .WithMany(p => p.CultivoInfectologico)
                    .HasForeignKey(d => d.InfectologicoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CultivoIn__Infec__77DFC722");
            });

            builder.Entity<Diagnostico>(entity =>
            {
                entity.Property(e => e.FechaAdd).HasColumnType("datetime");

                entity.Property(e => e.FechaDelete).HasColumnType("datetime");

                entity.Property(e => e.FechaUpdate).HasColumnType("datetime");

                entity.Property(e => e.Resumen)
                    .IsRequired()
                    .HasMaxLength(800);

                entity.Property(e => e.UserNameAdd)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserNameDelete).HasMaxLength(50);

                entity.Property(e => e.UserNameUpdate).HasMaxLength(50);
            });

            builder.Entity<EnzimasCardiacas>(entity =>
            {
                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            builder.Entity<EstadoInfectologico>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<Gasometria>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<GeneralHMN>(entity =>
            {
                entity.ToTable("GeneralHMN");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<Hematologico>(entity =>
            {
                entity.Property(e => e.CrasisFibrinoginos)
                    .HasColumnName("Crasis_Fibrinoginos")
                    .HasColumnType("numeric(10, 4)");

                entity.Property(e => e.CrasisTp)
                    .HasColumnName("Crasis_TP")
                    .HasColumnType("numeric(10, 4)");

                entity.Property(e => e.CrasisTtpa)
                    .HasColumnName("Crasis_TTPA")
                    .HasColumnType("numeric(10, 4)");

                entity.Property(e => e.DateAdd).HasColumnType("datetime");

                entity.Property(e => e.DateDelete).HasColumnType("datetime");

                entity.Property(e => e.DosisVitaminaK).HasColumnType("numeric(10, 4)");

                entity.Property(e => e.Eventos).HasMaxLength(800);

                entity.Property(e => e.FechaVitaminaK).HasColumnType("datetime");

                entity.Property(e => e.HemogramaHb)
                    .HasColumnName("Hemograma_HB")
                    .HasColumnType("numeric(10, 4)");

                entity.Property(e => e.HemogramaHtc)
                    .HasColumnName("Hemograma_HTC")
                    .HasColumnType("numeric(10, 4)");

                entity.Property(e => e.HemogramaPai)
                    .HasColumnName("Hemograma_PAI")
                    .HasColumnType("numeric(10, 4)");

                entity.Property(e => e.HemogramaPlt)
                    .HasColumnName("Hemograma_PLT")
                    .HasColumnType("numeric(10, 4)");

                entity.Property(e => e.LugarSangrado).HasMaxLength(200);

                entity.Property(e => e.Planes).HasMaxLength(800);

                entity.Property(e => e.TransfusionesCrio)
                    .HasColumnName("Transfusiones_CRIO")
                    .HasColumnType("numeric(10, 4)");

                entity.Property(e => e.TransfusionesGrc)
                    .HasColumnName("Transfusiones_GRC")
                    .HasColumnType("numeric(10, 4)");

                entity.Property(e => e.TransfusionesPfc)
                    .HasColumnName("Transfusiones_PFC")
                    .HasColumnType("numeric(10, 4)");

                entity.Property(e => e.TransfusionesPlt)
                    .HasColumnName("Transfusiones_PLT")
                    .HasColumnType("numeric(10, 4)");

                entity.Property(e => e.UserAdd)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserDelete).HasMaxLength(50);
            });

            builder.Entity<Hisopado>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<HisopadoInfectologico>(entity =>
            {
                entity.HasKey(e => new { e.InfectologicoId, e.HisopadoId })
                    .HasName("PK__Hisopado__D71BDF4652E1487A");

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.HasOne(d => d.Hisopado)
                    .WithMany(p => p.HisopadoInfectologico)
                    .HasForeignKey(d => d.HisopadoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__HisopadoI__Hisop__025D5595");

                entity.HasOne(d => d.Infectologico)
                    .WithMany(p => p.HisopadoInfectologico)
                    .HasForeignKey(d => d.InfectologicoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__HisopadoI__Infec__0169315C");
            });

            builder.Entity<HMN>(entity =>
            {
                entity.ToTable("HMN");

                entity.Property(e => e.DateAdd).HasColumnType("datetime");

                entity.Property(e => e.DateDelete).HasColumnType("datetime");

                entity.Property(e => e.Eventos).HasMaxLength(800);

                entity.Property(e => e.FormulacionDialisisPeritoneal).HasMaxLength(800);

                entity.Property(e => e.Planes).HasMaxLength(800);

                entity.Property(e => e.UserAdd)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserDelete).HasMaxLength(50);
            });

            builder.Entity<HMNBalanceHidricoHMN>(entity =>
            {
                entity.HasKey(e => new { e.HMNid, e.BalanceHidricoHMNid })
                    .HasName("PK__HMNBalan__A348F97E4C7C1C7B");

                entity.ToTable("HMNBalanceHidricoHMN");

                entity.Property(e => e.HMNid).HasColumnName("HMNId");

                entity.Property(e => e.BalanceHidricoHMNid).HasColumnName("BalanceHidricoHMNId");

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.BalanceHidricoHMN)
                    .WithMany(p => p.HMNbalanceHidricoHMN)
                    .HasForeignKey(d => d.BalanceHidricoHMNid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__HMNBalanc__Balan__4AA30C57");

                entity.HasOne(d => d.HMN)
                    .WithMany(p => p.HMNbalanceHidricoHMN)
                    .HasForeignKey(d => d.HMNid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__HMNBalanc__HMNId__49AEE81E");
            });

            builder.Entity<HMNGeneralHMN>(entity =>
            {
                entity.HasKey(e => new { e.HMNid, e.GeneralHMNid })
                    .HasName("PK__HMNGener__024982B4B1B71A57");

                entity.ToTable("HMNGeneralHMN");

                entity.Property(e => e.HMNid).HasColumnName("HMNId");

                entity.Property(e => e.GeneralHMNid).HasColumnName("GeneralHMNId");

                entity.Property(e => e.Formulacion).HasMaxLength(800);

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.GeneralHMN)
                    .WithMany(p => p.HMNgeneralHMN)
                    .HasForeignKey(d => d.GeneralHMNid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__HMNGenera__Gener__44EA3301");

                entity.HasOne(d => d.HMN)
                    .WithMany(p => p.HMNgeneralHMN)
                    .HasForeignKey(d => d.HMNid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__HMNGenera__HMNId__43F60EC8");
            });

            builder.Entity<HMNLaboratorioHMN>(entity =>
            {
                entity.HasKey(e => new { e.HMNid, e.LaboratorioHMNid })
                    .HasName("PK__HMNLabor__98EC19448247FD49");

                entity.ToTable("HMNLaboratorioHMN");

                entity.Property(e => e.HMNid).HasColumnName("HMNId");

                entity.Property(e => e.LaboratorioHMNid).HasColumnName("LaboratorioHMNId");

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.HMN)
                    .WithMany(p => p.HMNlaboratorioHMN)
                    .HasForeignKey(d => d.HMNid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__HMNLabora__HMNId__52442E1F");

                entity.HasOne(d => d.LaboratorioHMN)
                    .WithMany(p => p.HMNlaboratorioHMN)
                    .HasForeignKey(d => d.LaboratorioHMNid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__HMNLabora__Labor__53385258");
            });

            builder.Entity<ImagenSNC>(entity =>
            {
                entity.ToTable("ImagenSNC");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<Infectologico>(entity =>
            {
                entity.Property(e => e.DateAdd).HasColumnType("datetime");

                entity.Property(e => e.DateDelete).HasColumnType("datetime");

                entity.Property(e => e.Eventos).HasMaxLength(800);

                entity.Property(e => e.Interconsulta).HasMaxLength(800);

                entity.Property(e => e.Planes).HasMaxLength(800);

                entity.Property(e => e.UserAdd)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserDelete).HasMaxLength(50);

                entity.HasOne(d => d.EstadoInfectologico)
                    .WithMany(p => p.Infectologico)
                    .HasForeignKey(d => d.EstadoInfectologicoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Infectolo__Estad__703EA55A");
            });

            builder.Entity<Inotropico>(entity =>
            {
                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            builder.Entity<LaboratorioHMN>(entity =>
            {
                entity.ToTable("LaboratorioHMN");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);

                entity.HasOne(d => d.Categoria)
                    .WithMany(p => p.LaboratorioHMN)
                    .HasForeignKey(d => d.CategoriaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Laborator__Categ__4F67C174");
            });

            builder.Entity<LaboratorioSNC>(entity =>
            {
                entity.ToTable("LaboratorioSNC");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<MedicacionSNC>(entity =>
            {
                entity.ToTable("MedicacionSNC");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<MedicamentoSedacion>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<Modalidad>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<MovimientoCamaPaciente>(entity =>
            {
                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.TipoMovimientoId)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Cama)
                    .WithMany(p => p.MovimientoCamaPaciente)
                    .HasForeignKey(d => d.CamaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Movimient__CamaI__662B2B3B");
            });

            builder.Entity<Paciente>(entity =>
            {
                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FechaNacimiento).HasColumnType("datetime");

                entity.Property(e => e.GrupoSanguineo).HasMaxLength(3);

                entity.Property(e => e.Nombres)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.NroDocumento).HasMaxLength(80);

                entity.Property(e => e.Peso).HasColumnType("numeric(10, 4)");

                entity.Property(e => e.Sexo)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            builder.Entity<PacienteTablaHistorica>(entity =>
            {
                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FechaAdd).HasColumnType("datetime");

                entity.Property(e => e.FechaNacimiento).HasColumnType("datetime");

                entity.Property(e => e.GrupoSanguineo).HasMaxLength(3);

                entity.Property(e => e.Nombres)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.NroDocumento).HasMaxLength(80);

                entity.Property(e => e.Peso).HasColumnType("numeric(10, 4)");

                entity.Property(e => e.Sexo)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.UserNameAdd)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            builder.Entity<ParametrosGasometria>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<ParametrosSopRespiratorio>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<PRMS>(entity =>
            {
                entity.ToTable("PRMS");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre).HasMaxLength(200);
            });

            builder.Entity<Sedacion>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<SensibilidadCultivoInfectologico>(entity =>
            {
                entity.Property(e => e.Sensibilidad)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.Cultivo)
                    .WithMany(p => p.SensibilidadCultivoInfectologico)
                    .HasForeignKey(d => d.CultivoId)
                    .HasConstraintName("FK__Sensibili__Culti__147C05D0");

                entity.HasOne(d => d.Infectologico)
                    .WithMany(p => p.SensibilidadCultivoInfectologico)
                    .HasForeignKey(d => d.InfectologicoId)
                    .HasConstraintName("FK__Sensibili__Infec__1387E197");
            });

            builder.Entity<SNC>(entity =>
            {
                entity.ToTable("SNC");

                entity.Property(e => e.DateAdd).HasColumnType("datetime");

                entity.Property(e => e.DateDelete).HasColumnType("datetime");

                entity.Property(e => e.Eventos).HasMaxLength(800);

                entity.Property(e => e.Planes).HasMaxLength(800);

                entity.Property(e => e.UserAdd)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserDelete).HasMaxLength(50);

                entity.Property(e => e.ValorSedacion).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.Sedacion)
                    .WithMany(p => p.SNC)
                    .HasForeignKey(d => d.SedacionId)
                    .HasConstraintName("FK__SNC__SedacionId__5CF6C6BC");
            });

            builder.Entity<SNCanticonvulsionante>(entity =>
            {
                entity.HasKey(e => new { e.SNCid, e.AnticonvulsionanteId })
                    .HasName("PK__SNCAntic__14949542CAC7766A");

                entity.ToTable("SNCAnticonvulsionante");

                entity.Property(e => e.SNCid).HasColumnName("SNCId");

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.Anticonvulsionante)
                    .WithMany(p => p.SNCanticonvulsionante)
                    .HasForeignKey(d => d.AnticonvulsionanteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCAntico__Antic__61BB7BD9");

                entity.HasOne(d => d.SNC)
                    .WithMany(p => p.SNCanticonvulsionante)
                    .HasForeignKey(d => d.SNCid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCAntico__SNCId__62AFA012");
            });

            builder.Entity<SNCaspectoGeneralSNC>(entity =>
            {
                entity.HasKey(e => new { e.SNCid, e.AspectoGeneralId })
                    .HasName("PK__SNCAspec__E242B3D755EFDC03");

                entity.ToTable("SNCAspectoGeneralSNC");

                entity.Property(e => e.SNCid).HasColumnName("SNCId");

                entity.HasOne(d => d.AspectoGeneral)
                    .WithMany(p => p.SNCaspectoGeneralSNC)
                    .HasForeignKey(d => d.AspectoGeneralId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCAspect__Aspec__658C0CBD");

                entity.HasOne(d => d.SNC)
                    .WithMany(p => p.SNCaspectoGeneralSNC)
                    .HasForeignKey(d => d.SNCid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCAspect__SNCId__668030F6");
            });

            builder.Entity<SNCimagenesSNC>(entity =>
            {
                entity.HasKey(e => new { e.SNCid, e.ImagenesSNCid })
                    .HasName("PK__SNCImage__50829555167DF4A4");

                entity.ToTable("SNCImagenesSNC");

                entity.Property(e => e.SNCid).HasColumnName("SNCId");

                entity.Property(e => e.ImagenesSNCid).HasColumnName("ImagenesSNCId");

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.ImagenesSNC)
                    .WithMany(p => p.SNCimagenesSNC)
                    .HasForeignKey(d => d.ImagenesSNCid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCImagen__Image__695C9DA1");

                entity.HasOne(d => d.SNC)
                    .WithMany(p => p.SNCimagenesSNC)
                    .HasForeignKey(d => d.SNCid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCImagen__SNCId__6A50C1DA");
            });

            builder.Entity<SNClaboratorioSNC>(entity =>
            {
                entity.HasKey(e => new { e.SNCid, e.LaboratorioSNCid })
                    .HasName("PK__SNCLabor__07B3320362DFB7BA");

                entity.ToTable("SNCLaboratorioSNC");

                entity.Property(e => e.SNCid).HasColumnName("SNCId");

                entity.Property(e => e.LaboratorioSNCid).HasColumnName("LaboratorioSNCId");

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.LaboratorioSNC)
                    .WithMany(p => p.SNClaboratorioSNC)
                    .HasForeignKey(d => d.LaboratorioSNCid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCLabora__Labor__6D2D2E85");

                entity.HasOne(d => d.SNC)
                    .WithMany(p => p.SNClaboratorioSNC)
                    .HasForeignKey(d => d.SNCid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCLabora__SNCId__6E2152BE");
            });

            builder.Entity<SNCmedicacionSNC>(entity =>
            {
                entity.HasKey(e => new { e.SNCid, e.MedicacionSNCid })
                    .HasName("PK__SNCMedic__66B5060C9B272783");

                entity.ToTable("SNCMedicacionSNC");

                entity.Property(e => e.SNCid).HasColumnName("SNCId");

                entity.Property(e => e.MedicacionSNCid).HasColumnName("MedicacionSNCId");

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.MedicacionSNC)
                    .WithMany(p => p.SNCmedicacionSNC)
                    .HasForeignKey(d => d.MedicacionSNCid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCMedica__Medic__70FDBF69");

                entity.HasOne(d => d.SNC)
                    .WithMany(p => p.SNCmedicacionSNC)
                    .HasForeignKey(d => d.SNCid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCMedica__SNCId__71F1E3A2");
            });

            builder.Entity<SNCmedicamentoSedacion>(entity =>
            {
                entity.HasKey(e => new { e.SNCid, e.MedicamentoSedacionId })
                    .HasName("PK__SNCMedic__C3F68DC6EB48A987");

                entity.ToTable("SNCMedicamentoSedacion");

                entity.Property(e => e.SNCid).HasColumnName("SNCId");

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.MedicamentoSedacion)
                    .WithMany(p => p.SNCmedicamentoSedacion)
                    .HasForeignKey(d => d.MedicamentoSedacionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCMedica__Medic__74CE504D");

                entity.HasOne(d => d.SNC)
                    .WithMany(p => p.SNCmedicamentoSedacion)
                    .HasForeignKey(d => d.SNCid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCMedica__SNCId__75C27486");
            });

            builder.Entity<SNCsxAbstinenciaSNC>(entity =>
            {
                entity.HasKey(e => new { e.SNCid, e.SxAbstinenciaSNCid })
                    .HasName("PK__SNCSxAbs__97DECFADF0F8DC3C");

                entity.ToTable("SNCSxAbstinenciaSNC");

                entity.Property(e => e.SNCid).HasColumnName("SNCId");

                entity.Property(e => e.SxAbstinenciaSNCid).HasColumnName("SxAbstinenciaSNCId");

                entity.Property(e => e.Valor).HasColumnType("numeric(10, 4)");

                entity.HasOne(d => d.SNC)
                    .WithMany(p => p.SNCsxAbstinenciaSNC)
                    .HasForeignKey(d => d.SNCid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCSxAbst__SNCId__789EE131");

                entity.HasOne(d => d.SxAbstinenciaSNC)
                    .WithMany(p => p.SNCsxAbstinenciaSNC)
                    .HasForeignKey(d => d.SxAbstinenciaSNCid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SNCSxAbst__SxAbs__7993056A");
            });

            builder.Entity<SoporteRespiratorio>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<SxAbstinenciaSNC>(entity =>
            {
                entity.ToTable("SxAbstinenciaSNC");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            builder.Entity<Ventilacion>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            base.OnModelCreating(builder);
        }
    }
}