﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using System;
using System.Collections.Generic;

namespace HistorialClinico.Infrastructure.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Anticonvulsionante",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anticonvulsionante", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspectoGeneralSNC",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspectoGeneralSNC", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    SecurityStamp = table.Column<string>(nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BalanceHidricoHMN",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BalanceHidricoHMN", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cama",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cama", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CategoriaLaboratorio",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoriaLaboratorio", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cultivo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cultivo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EnzimasCardiacas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnzimasCardiacas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EstadoInfectologico",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EstadoInfectologico", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Gasometria",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gasometria", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GeneralHMN",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Formulacion = table.Column<bool>(nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false),
                    Valor = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralHMN", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Hematologico",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Crasis_Fibrinoginos = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    Crasis_TP = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    Crasis_TTPA = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    DateAdd = table.Column<DateTime>(type: "timestamp", nullable: false),
                    DateDelete = table.Column<DateTime>(type: "timestamp", nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    DosisVitaminaK = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    Eventos = table.Column<string>(maxLength: 800, nullable: true),
                    FechaVitaminaK = table.Column<DateTime>(type: "timestamp", nullable: true),
                    Hemograma_HB = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    Hemograma_HTC = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    Hemograma_PAI = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    Hemograma_PLT = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    LugarSangrado = table.Column<string>(maxLength: 200, nullable: true),
                    PacienteId = table.Column<int>(nullable: false),
                    Planes = table.Column<string>(maxLength: 800, nullable: true),
                    SangradoActivo = table.Column<bool>(nullable: false),
                    Transfusiones = table.Column<bool>(nullable: false),
                    Transfusiones_CRIO = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    Transfusiones_GRC = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    Transfusiones_PFC = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    Transfusiones_PLT = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    UserAdd = table.Column<string>(maxLength: 50, nullable: false),
                    UserDelete = table.Column<string>(maxLength: 50, nullable: true),
                    VitaminaK = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hematologico", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Hisopado",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hisopado", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ImagenSNC",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImagenSNC", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Inotropicos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inotropicos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LaboratorioSNC",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LaboratorioSNC", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MedicacionSNC",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MedicacionSNC", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MedicamentoSedacion",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MedicamentoSedacion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Modalidad",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Modalidad", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ParametrosGasometria",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParametrosGasometria", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ParametrosSopRespiratorio",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false),
                    Orden = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParametrosSopRespiratorio", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PRMS",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PRMS", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sedacion",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false),
                    RequeridoValor = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sedacion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SoporteRespiratorio",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false),
                    Parametros = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SoporteRespiratorio", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SxAbstinenciaSNC",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SxAbstinenciaSNC", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ventilacion",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ventilacion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LaboratorioHMN",
                columns: table => new
                {
                    Id = table.Column<int>(type: "serial", nullable: false),
                    CategoriaId = table.Column<int>(nullable: false),
                    Nombre = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LaboratorioHMN", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Laborator__Categ__4F67C174",
                        column: x => x.CategoriaId,
                        principalTable: "CategoriaLaboratorio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Paciente",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Apellidos = table.Column<string>(maxLength: 50, nullable: false),
                    FechaNacimiento = table.Column<DateTime>(type: "timestamp", nullable: true),
                    GrupoSanguineo = table.Column<string>(maxLength: 3, nullable: true),
                    Nombres = table.Column<string>(maxLength: 50, nullable: false),
                    NroDocumento = table.Column<string>(maxLength: 80, nullable: true),
                    Peso = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    PRMSId = table.Column<int>(nullable: true),
                    Sexo = table.Column<string>(unicode: false, maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Paciente", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Paciente__PRMSId__44CA3770",
                        column: x => x.PRMSId,
                        principalTable: "PRMS",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApCardiovascular",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateAdd = table.Column<DateTime>(type: "timestamp", nullable: false),
                    DateDelete = table.Column<DateTime>(type: "timestamp", nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    EstadoId = table.Column<string>(maxLength: 1, nullable: false),
                    EvaluacionCardiologica = table.Column<string>(maxLength: 800, nullable: true),
                    Eventos = table.Column<string>(maxLength: 800, nullable: true),
                    PacienteId = table.Column<int>(nullable: false),
                    Planes = table.Column<string>(maxLength: 800, nullable: true),
                    UserAdd = table.Column<string>(maxLength: 50, nullable: false),
                    UserDelete = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApCardiovascular", x => x.Id);
                    table.ForeignKey(
                        name: "FK__ApCardiov__Pacie__59904A2C",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApRespiratorio",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateAdd = table.Column<DateTime>(type: "timestamp", nullable: false),
                    DateDelete = table.Column<DateTime>(type: "timestamp", nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    Eventos = table.Column<string>(maxLength: 800, nullable: true),
                    GasometriaId = table.Column<int>(nullable: true),
                    Manejo = table.Column<string>(maxLength: 800, nullable: true),
                    ModalidadId = table.Column<int>(nullable: true),
                    PacienteId = table.Column<int>(nullable: false),
                    Planes = table.Column<string>(maxLength: 800, nullable: true),
                    SoporteRespiratorioId = table.Column<int>(nullable: true),
                    UserAdd = table.Column<string>(maxLength: 50, nullable: false),
                    UserDelete = table.Column<string>(maxLength: 50, nullable: true),
                    ValorSoporteResp = table.Column<string>(maxLength: 80, nullable: true),
                    VentilacionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApRespiratorio", x => x.Id);
                    table.ForeignKey(
                        name: "FK__ApRespira__Gasom__3DB3258D",
                        column: x => x.GasometriaId,
                        principalTable: "Gasometria",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__ApRespira__Modal__3CBF0154",
                        column: x => x.ModalidadId,
                        principalTable: "Modalidad",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__ApRespira__Pacie__39E294A9",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__ApRespira__Sopor__3AD6B8E2",
                        column: x => x.SoporteRespiratorioId,
                        principalTable: "SoporteRespiratorio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__ApRespira__Venti__3BCADD1B",
                        column: x => x.VentilacionId,
                        principalTable: "Ventilacion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CamaPaciente",
                columns: table => new
                {
                    CamaId = table.Column<int>(nullable: false),
                    PacienteId = table.Column<int>(nullable: false),
                    FechaIngreso = table.Column<DateTime>(type: "timestamp", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__CamaPaci__9F6B910C90775F76", x => new { x.CamaId, x.PacienteId });
                    table.ForeignKey(
                        name: "FK__CamaPacie__CamaI__76619304",
                        column: x => x.CamaId,
                        principalTable: "Cama",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__CamaPacie__Pacie__7755B73D",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cirugia",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CualProcedimiento = table.Column<string>(maxLength: 800, nullable: true),
                    DateAdd = table.Column<DateTime>(type: "timestamp", nullable: false),
                    DateDelete = table.Column<DateTime>(type: "timestamp", nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    Hallazgos = table.Column<string>(maxLength: 800, nullable: true),
                    OtrasAcotaciones = table.Column<string>(maxLength: 800, nullable: true),
                    PacienteId = table.Column<int>(nullable: false),
                    Procedimiento = table.Column<bool>(nullable: false),
                    Tecnica = table.Column<string>(maxLength: 800, nullable: true),
                    TuvoCirugia = table.Column<bool>(nullable: false),
                    UserAdd = table.Column<string>(maxLength: 50, nullable: false),
                    UserDelete = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cirugia", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Cirugia__Pacient__57C7FD4B",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactoPaciente",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    FechaAdd = table.Column<DateTime>(type: "timestamp", nullable: false),
                    FechaDelete = table.Column<DateTime>(type: "timestamp", nullable: true),
                    FechaUpdate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    NombreContacto = table.Column<string>(maxLength: 200, nullable: false),
                    NroContacto = table.Column<string>(maxLength: 50, nullable: true),
                    PacienteId = table.Column<int>(nullable: false),
                    TipoContactoId = table.Column<string>(unicode: false, maxLength: 1, nullable: true),
                    UserNameAdd = table.Column<string>(maxLength: 50, nullable: false),
                    UserNameDelete = table.Column<string>(maxLength: 50, nullable: true),
                    UserNameUpdate = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactoPaciente", x => x.Id);
                    table.ForeignKey(
                        name: "FK__ContactoP__Pacie__29221CFB",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Diagnostico",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    FechaAdd = table.Column<DateTime>(type: "timestamp", nullable: false),
                    FechaDelete = table.Column<DateTime>(type: "timestamp", nullable: true),
                    FechaUpdate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    PacienteId = table.Column<int>(nullable: false),
                    Resumen = table.Column<string>(maxLength: 800, nullable: false),
                    UserNameAdd = table.Column<string>(maxLength: 50, nullable: false),
                    UserNameDelete = table.Column<string>(maxLength: 50, nullable: true),
                    UserNameUpdate = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Diagnostico", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Diagnosti__Pacie__3C34F16F",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HMN",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateAdd = table.Column<DateTime>(type: "timestamp", nullable: false),
                    DateDelete = table.Column<DateTime>(type: "timestamp", nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    DialisisPeritoneal = table.Column<bool>(nullable: false),
                    Eventos = table.Column<string>(maxLength: 800, nullable: true),
                    FormulacionDialisisPeritoneal = table.Column<string>(maxLength: 800, nullable: true),
                    PacienteId = table.Column<int>(nullable: false),
                    Planes = table.Column<string>(maxLength: 800, nullable: true),
                    UserAdd = table.Column<string>(maxLength: 50, nullable: false),
                    UserDelete = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HMN", x => x.Id);
                    table.ForeignKey(
                        name: "FK__HMN__PacienteId__3F3159AB",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Infectologico",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DateAdd = table.Column<DateTime>(type: "timestamp", nullable: false),
                    DateDelete = table.Column<DateTime>(type: "timestamp", nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    EstadoInfectologicoId = table.Column<int>(nullable: false),
                    Eventos = table.Column<string>(maxLength: 800, nullable: true),
                    Interconsulta = table.Column<string>(maxLength: 800, nullable: true),
                    PacienteId = table.Column<int>(nullable: false),
                    Planes = table.Column<string>(maxLength: 800, nullable: true),
                    UserAdd = table.Column<string>(maxLength: 50, nullable: false),
                    UserDelete = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Infectologico", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Infectolo__Estad__703EA55A",
                        column: x => x.EstadoInfectologicoId,
                        principalTable: "EstadoInfectologico",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Infectolo__Pacie__6F4A8121",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MovimientoCamaPaciente",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CamaId = table.Column<int>(nullable: false),
                    Fecha = table.Column<DateTime>(type: "timestamp", nullable: false),
                    PacienteId = table.Column<int>(nullable: false),
                    TipoMovimientoId = table.Column<string>(unicode: false, maxLength: 3, nullable: false),
                    UserName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovimientoCamaPaciente", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Movimient__CamaI__662B2B3B",
                        column: x => x.CamaId,
                        principalTable: "Cama",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Movimient__Pacie__671F4F74",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PacienteTablaHistorica",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Apellidos = table.Column<string>(maxLength: 50, nullable: false),
                    FechaAdd = table.Column<DateTime>(type: "timestamp", nullable: false),
                    FechaNacimiento = table.Column<DateTime>(type: "timestamp", nullable: true),
                    GrupoSanguineo = table.Column<string>(maxLength: 3, nullable: true),
                    Nombres = table.Column<string>(maxLength: 50, nullable: false),
                    NroDocumento = table.Column<string>(maxLength: 80, nullable: true),
                    PacienteId = table.Column<int>(nullable: false),
                    Peso = table.Column<decimal>(type: "numeric(10, 4)", nullable: true),
                    PRMSId = table.Column<int>(nullable: true),
                    Sexo = table.Column<string>(unicode: false, maxLength: 1, nullable: false),
                    UserNameAdd = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PacienteTablaHistorica", x => x.Id);
                    table.ForeignKey(
                        name: "FK__PacienteT__Pacie__1AD3FDA4",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__PacienteT__PRMSI__46B27FE2",
                        column: x => x.PRMSId,
                        principalTable: "PRMS",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SNC",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ConocidoConvulsionador = table.Column<bool>(nullable: false),
                    DateAdd = table.Column<DateTime>(type: "timestamp", nullable: false),
                    DateDelete = table.Column<DateTime>(type: "timestamp", nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    Eventos = table.Column<string>(maxLength: 800, nullable: true),
                    PacienteId = table.Column<int>(nullable: false),
                    Planes = table.Column<string>(maxLength: 800, nullable: true),
                    SedacionId = table.Column<int>(nullable: true),
                    SxAbstinencia = table.Column<bool>(nullable: false),
                    UserAdd = table.Column<string>(maxLength: 50, nullable: false),
                    UserDelete = table.Column<string>(maxLength: 50, nullable: true),
                    ValorSedacion = table.Column<decimal>(type: "numeric(10, 4)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SNC", x => x.Id);
                    table.ForeignKey(
                        name: "FK__SNC__PacienteId__5DEAEAF5",
                        column: x => x.PacienteId,
                        principalTable: "Paciente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__SNC__SedacionId__5CF6C6BC",
                        column: x => x.SedacionId,
                        principalTable: "Sedacion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApCardiovascularEnzimasCardiacas",
                columns: table => new
                {
                    ApCardiovascularId = table.Column<int>(nullable: false),
                    EnzimasCardiacasId = table.Column<int>(nullable: false),
                    Curva = table.Column<string>(maxLength: 1, nullable: false),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__ApCardio__CAA8F0753B57158A", x => new { x.ApCardiovascularId, x.EnzimasCardiacasId });
                    table.ForeignKey(
                        name: "FK__ApCardiov__ApCar__6501FCD8",
                        column: x => x.ApCardiovascularId,
                        principalTable: "ApCardiovascular",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__ApCardiov__Enzim__65F62111",
                        column: x => x.EnzimasCardiacasId,
                        principalTable: "EnzimasCardiacas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApCardiovascularInotropicos",
                columns: table => new
                {
                    ApCardiovascularId = table.Column<int>(nullable: false),
                    InotropicosId = table.Column<int>(nullable: false),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__ApCardio__B2DCFF4091DCE8AE", x => new { x.ApCardiovascularId, x.InotropicosId });
                    table.ForeignKey(
                        name: "FK__ApCardiov__ApCar__61316BF4",
                        column: x => x.ApCardiovascularId,
                        principalTable: "ApCardiovascular",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__ApCardiov__Inotr__6225902D",
                        column: x => x.InotropicosId,
                        principalTable: "Inotropicos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApRespiratorioParametros",
                columns: table => new
                {
                    ApRespiratorioId = table.Column<int>(nullable: false),
                    ParametroId = table.Column<int>(nullable: false),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__ApRespir__7BBA3629EC2A25E2", x => new { x.ApRespiratorioId, x.ParametroId });
                    table.ForeignKey(
                        name: "FK__ApRespira__ApRes__408F9238",
                        column: x => x.ApRespiratorioId,
                        principalTable: "ApRespiratorio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__ApRespira__Param__4183B671",
                        column: x => x.ParametroId,
                        principalTable: "ParametrosSopRespiratorio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApRespiratorioParametrosGasometria",
                columns: table => new
                {
                    ApRespiratorioId = table.Column<int>(nullable: false),
                    ParametroId = table.Column<int>(nullable: false),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__ApRespir__7BBA3629441C71F0", x => new { x.ApRespiratorioId, x.ParametroId });
                    table.ForeignKey(
                        name: "FK__ApRespira__ApRes__4460231C",
                        column: x => x.ApRespiratorioId,
                        principalTable: "ApRespiratorio",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__ApRespira__Param__45544755",
                        column: x => x.ParametroId,
                        principalTable: "ParametrosGasometria",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HMNBalanceHidricoHMN",
                columns: table => new
                {
                    HMNId = table.Column<int>(nullable: false),
                    BalanceHidricoHMNId = table.Column<int>(nullable: false),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__HMNBalan__A348F97E4C7C1C7B", x => new { x.HMNId, x.BalanceHidricoHMNId });
                    table.ForeignKey(
                        name: "FK__HMNBalanc__Balan__4AA30C57",
                        column: x => x.BalanceHidricoHMNId,
                        principalTable: "BalanceHidricoHMN",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__HMNBalanc__HMNId__49AEE81E",
                        column: x => x.HMNId,
                        principalTable: "HMN",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HMNGeneralHMN",
                columns: table => new
                {
                    HMNId = table.Column<int>(nullable: false),
                    GeneralHMNId = table.Column<int>(nullable: false),
                    Formulacion = table.Column<string>(maxLength: 800, nullable: true),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__HMNGener__024982B4B1B71A57", x => new { x.HMNId, x.GeneralHMNId });
                    table.ForeignKey(
                        name: "FK__HMNGenera__Gener__44EA3301",
                        column: x => x.GeneralHMNId,
                        principalTable: "GeneralHMN",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__HMNGenera__HMNId__43F60EC8",
                        column: x => x.HMNId,
                        principalTable: "HMN",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HMNLaboratorioHMN",
                columns: table => new
                {
                    HMNId = table.Column<int>(nullable: false),
                    LaboratorioHMNId = table.Column<int>(nullable: false),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__HMNLabor__98EC19448247FD49", x => new { x.HMNId, x.LaboratorioHMNId });
                    table.ForeignKey(
                        name: "FK__HMNLabora__HMNId__52442E1F",
                        column: x => x.HMNId,
                        principalTable: "HMN",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__HMNLabora__Labor__53385258",
                        column: x => x.LaboratorioHMNId,
                        principalTable: "LaboratorioHMN",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CoberturaAntibioticos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AjustadoClearence = table.Column<bool>(nullable: false),
                    Antibiotico = table.Column<string>(maxLength: 100, nullable: false),
                    Dosis = table.Column<decimal>(type: "numeric(10, 4)", nullable: false),
                    FechaInicio = table.Column<DateTime>(type: "timestamp", nullable: false),
                    FechaSuspension = table.Column<DateTime>(type: "timestamp", nullable: true),
                    InfectologicoId = table.Column<int>(nullable: false),
                    Unidad = table.Column<string>(maxLength: 80, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoberturaAntibioticos", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Cobertura__Infec__731B1205",
                        column: x => x.InfectologicoId,
                        principalTable: "Infectologico",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CultivoInfectologico",
                columns: table => new
                {
                    InfectologicoId = table.Column<int>(nullable: false),
                    CultivoId = table.Column<int>(nullable: false),
                    Fecha = table.Column<DateTime>(type: "timestamp", nullable: false),
                    Resultado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__CultivoI__897F900D9D51FCFA", x => new { x.InfectologicoId, x.CultivoId });
                    table.ForeignKey(
                        name: "FK__CultivoIn__Culti__78D3EB5B",
                        column: x => x.CultivoId,
                        principalTable: "Cultivo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__CultivoIn__Infec__77DFC722",
                        column: x => x.InfectologicoId,
                        principalTable: "Infectologico",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HisopadoInfectologico",
                columns: table => new
                {
                    InfectologicoId = table.Column<int>(nullable: false),
                    HisopadoId = table.Column<int>(nullable: false),
                    Fecha = table.Column<DateTime>(type: "timestamp", nullable: false),
                    Resultado = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Hisopado__D71BDF4652E1487A", x => new { x.InfectologicoId, x.HisopadoId });
                    table.ForeignKey(
                        name: "FK__HisopadoI__Hisop__025D5595",
                        column: x => x.HisopadoId,
                        principalTable: "Hisopado",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__HisopadoI__Infec__0169315C",
                        column: x => x.InfectologicoId,
                        principalTable: "Infectologico",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SensibilidadCultivoInfectologico",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CultivoId = table.Column<int>(nullable: true),
                    InfectologicoId = table.Column<int>(nullable: true),
                    Sensibilidad = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SensibilidadCultivoInfectologico", x => x.Id);
                    table.ForeignKey(
                        name: "FK__Sensibili__Culti__147C05D0",
                        column: x => x.CultivoId,
                        principalTable: "Cultivo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__Sensibili__Infec__1387E197",
                        column: x => x.InfectologicoId,
                        principalTable: "Infectologico",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SNCAnticonvulsionante",
                columns: table => new
                {
                    SNCId = table.Column<int>(nullable: false),
                    AnticonvulsionanteId = table.Column<int>(nullable: false),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__SNCAntic__14949542CAC7766A", x => new { x.SNCId, x.AnticonvulsionanteId });
                    table.ForeignKey(
                        name: "FK__SNCAntico__Antic__61BB7BD9",
                        column: x => x.AnticonvulsionanteId,
                        principalTable: "Anticonvulsionante",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__SNCAntico__SNCId__62AFA012",
                        column: x => x.SNCId,
                        principalTable: "SNC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SNCAspectoGeneralSNC",
                columns: table => new
                {
                    SNCId = table.Column<int>(nullable: false),
                    AspectoGeneralId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__SNCAspec__E242B3D755EFDC03", x => new { x.SNCId, x.AspectoGeneralId });
                    table.ForeignKey(
                        name: "FK__SNCAspect__Aspec__658C0CBD",
                        column: x => x.AspectoGeneralId,
                        principalTable: "AspectoGeneralSNC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__SNCAspect__SNCId__668030F6",
                        column: x => x.SNCId,
                        principalTable: "SNC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SNCImagenesSNC",
                columns: table => new
                {
                    SNCId = table.Column<int>(nullable: false),
                    ImagenesSNCId = table.Column<int>(nullable: false),
                    Fecha = table.Column<DateTime>(type: "timestamp", nullable: false),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__SNCImage__50829555167DF4A4", x => new { x.SNCId, x.ImagenesSNCId });
                    table.ForeignKey(
                        name: "FK__SNCImagen__Image__695C9DA1",
                        column: x => x.ImagenesSNCId,
                        principalTable: "ImagenSNC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__SNCImagen__SNCId__6A50C1DA",
                        column: x => x.SNCId,
                        principalTable: "SNC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SNCLaboratorioSNC",
                columns: table => new
                {
                    SNCId = table.Column<int>(nullable: false),
                    LaboratorioSNCId = table.Column<int>(nullable: false),
                    Fecha = table.Column<DateTime>(type: "timestamp", nullable: false),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__SNCLabor__07B3320362DFB7BA", x => new { x.SNCId, x.LaboratorioSNCId });
                    table.ForeignKey(
                        name: "FK__SNCLabora__Labor__6D2D2E85",
                        column: x => x.LaboratorioSNCId,
                        principalTable: "LaboratorioSNC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__SNCLabora__SNCId__6E2152BE",
                        column: x => x.SNCId,
                        principalTable: "SNC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SNCMedicacionSNC",
                columns: table => new
                {
                    SNCId = table.Column<int>(nullable: false),
                    MedicacionSNCId = table.Column<int>(nullable: false),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__SNCMedic__66B5060C9B272783", x => new { x.SNCId, x.MedicacionSNCId });
                    table.ForeignKey(
                        name: "FK__SNCMedica__Medic__70FDBF69",
                        column: x => x.MedicacionSNCId,
                        principalTable: "MedicacionSNC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__SNCMedica__SNCId__71F1E3A2",
                        column: x => x.SNCId,
                        principalTable: "SNC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SNCMedicamentoSedacion",
                columns: table => new
                {
                    SNCId = table.Column<int>(nullable: false),
                    MedicamentoSedacionId = table.Column<int>(nullable: false),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__SNCMedic__C3F68DC6EB48A987", x => new { x.SNCId, x.MedicamentoSedacionId });
                    table.ForeignKey(
                        name: "FK__SNCMedica__Medic__74CE504D",
                        column: x => x.MedicamentoSedacionId,
                        principalTable: "MedicamentoSedacion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__SNCMedica__SNCId__75C27486",
                        column: x => x.SNCId,
                        principalTable: "SNC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SNCSxAbstinenciaSNC",
                columns: table => new
                {
                    SNCId = table.Column<int>(nullable: false),
                    SxAbstinenciaSNCId = table.Column<int>(nullable: false),
                    Valor = table.Column<decimal>(type: "numeric(10, 4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__SNCSxAbs__97DECFADF0F8DC3C", x => new { x.SNCId, x.SxAbstinenciaSNCId });
                    table.ForeignKey(
                        name: "FK__SNCSxAbst__SNCId__789EE131",
                        column: x => x.SNCId,
                        principalTable: "SNC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK__SNCSxAbst__SxAbs__7993056A",
                        column: x => x.SxAbstinenciaSNCId,
                        principalTable: "SxAbstinenciaSNC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApCardiovascular_PacienteId",
                table: "ApCardiovascular",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_ApCardiovascularEnzimasCardiacas_EnzimasCardiacasId",
                table: "ApCardiovascularEnzimasCardiacas",
                column: "EnzimasCardiacasId");

            migrationBuilder.CreateIndex(
                name: "IX_ApCardiovascularInotropicos_InotropicosId",
                table: "ApCardiovascularInotropicos",
                column: "InotropicosId");

            migrationBuilder.CreateIndex(
                name: "IX_ApRespiratorio_GasometriaId",
                table: "ApRespiratorio",
                column: "GasometriaId");

            migrationBuilder.CreateIndex(
                name: "IX_ApRespiratorio_ModalidadId",
                table: "ApRespiratorio",
                column: "ModalidadId");

            migrationBuilder.CreateIndex(
                name: "IX_ApRespiratorio_PacienteId",
                table: "ApRespiratorio",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_ApRespiratorio_SoporteRespiratorioId",
                table: "ApRespiratorio",
                column: "SoporteRespiratorioId");

            migrationBuilder.CreateIndex(
                name: "IX_ApRespiratorio_VentilacionId",
                table: "ApRespiratorio",
                column: "VentilacionId");

            migrationBuilder.CreateIndex(
                name: "IX_ApRespiratorioParametros_ParametroId",
                table: "ApRespiratorioParametros",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_ApRespiratorioParametrosGasometria_ParametroId",
                table: "ApRespiratorioParametrosGasometria",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UQ__CamaPaci__765EAD0C3E3A5D66",
                table: "CamaPaciente",
                column: "CamaId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UQ__CamaPaci__9353C01E74CE701A",
                table: "CamaPaciente",
                column: "PacienteId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cirugia_PacienteId",
                table: "Cirugia",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_CoberturaAntibioticos_InfectologicoId",
                table: "CoberturaAntibioticos",
                column: "InfectologicoId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactoPaciente_PacienteId",
                table: "ContactoPaciente",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_CultivoInfectologico_CultivoId",
                table: "CultivoInfectologico",
                column: "CultivoId");

            migrationBuilder.CreateIndex(
                name: "IX_Diagnostico_PacienteId",
                table: "Diagnostico",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_HisopadoInfectologico_HisopadoId",
                table: "HisopadoInfectologico",
                column: "HisopadoId");

            migrationBuilder.CreateIndex(
                name: "IX_HMN_PacienteId",
                table: "HMN",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_HMNBalanceHidricoHMN_BalanceHidricoHMNId",
                table: "HMNBalanceHidricoHMN",
                column: "BalanceHidricoHMNId");

            migrationBuilder.CreateIndex(
                name: "IX_HMNGeneralHMN_GeneralHMNId",
                table: "HMNGeneralHMN",
                column: "GeneralHMNId");

            migrationBuilder.CreateIndex(
                name: "IX_HMNLaboratorioHMN_LaboratorioHMNId",
                table: "HMNLaboratorioHMN",
                column: "LaboratorioHMNId");

            migrationBuilder.CreateIndex(
                name: "IX_Infectologico_EstadoInfectologicoId",
                table: "Infectologico",
                column: "EstadoInfectologicoId");

            migrationBuilder.CreateIndex(
                name: "IX_Infectologico_PacienteId",
                table: "Infectologico",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_LaboratorioHMN_CategoriaId",
                table: "LaboratorioHMN",
                column: "CategoriaId");

            migrationBuilder.CreateIndex(
                name: "IX_MovimientoCamaPaciente_CamaId",
                table: "MovimientoCamaPaciente",
                column: "CamaId");

            migrationBuilder.CreateIndex(
                name: "IX_MovimientoCamaPaciente_PacienteId",
                table: "MovimientoCamaPaciente",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Paciente_PRMSId",
                table: "Paciente",
                column: "PRMSId");

            migrationBuilder.CreateIndex(
                name: "IX_PacienteTablaHistorica_PacienteId",
                table: "PacienteTablaHistorica",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_PacienteTablaHistorica_PRMSId",
                table: "PacienteTablaHistorica",
                column: "PRMSId");

            migrationBuilder.CreateIndex(
                name: "IX_SensibilidadCultivoInfectologico_CultivoId",
                table: "SensibilidadCultivoInfectologico",
                column: "CultivoId");

            migrationBuilder.CreateIndex(
                name: "IX_SensibilidadCultivoInfectologico_InfectologicoId",
                table: "SensibilidadCultivoInfectologico",
                column: "InfectologicoId");

            migrationBuilder.CreateIndex(
                name: "IX_SNC_PacienteId",
                table: "SNC",
                column: "PacienteId");

            migrationBuilder.CreateIndex(
                name: "IX_SNC_SedacionId",
                table: "SNC",
                column: "SedacionId");

            migrationBuilder.CreateIndex(
                name: "IX_SNCAnticonvulsionante_AnticonvulsionanteId",
                table: "SNCAnticonvulsionante",
                column: "AnticonvulsionanteId");

            migrationBuilder.CreateIndex(
                name: "IX_SNCAspectoGeneralSNC_AspectoGeneralId",
                table: "SNCAspectoGeneralSNC",
                column: "AspectoGeneralId");

            migrationBuilder.CreateIndex(
                name: "IX_SNCImagenesSNC_ImagenesSNCId",
                table: "SNCImagenesSNC",
                column: "ImagenesSNCId");

            migrationBuilder.CreateIndex(
                name: "IX_SNCLaboratorioSNC_LaboratorioSNCId",
                table: "SNCLaboratorioSNC",
                column: "LaboratorioSNCId");

            migrationBuilder.CreateIndex(
                name: "IX_SNCMedicacionSNC_MedicacionSNCId",
                table: "SNCMedicacionSNC",
                column: "MedicacionSNCId");

            migrationBuilder.CreateIndex(
                name: "IX_SNCMedicamentoSedacion_MedicamentoSedacionId",
                table: "SNCMedicamentoSedacion",
                column: "MedicamentoSedacionId");

            migrationBuilder.CreateIndex(
                name: "IX_SNCSxAbstinenciaSNC_SxAbstinenciaSNCId",
                table: "SNCSxAbstinenciaSNC",
                column: "SxAbstinenciaSNCId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApCardiovascularEnzimasCardiacas");

            migrationBuilder.DropTable(
                name: "ApCardiovascularInotropicos");

            migrationBuilder.DropTable(
                name: "ApRespiratorioParametros");

            migrationBuilder.DropTable(
                name: "ApRespiratorioParametrosGasometria");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "CamaPaciente");

            migrationBuilder.DropTable(
                name: "Cirugia");

            migrationBuilder.DropTable(
                name: "CoberturaAntibioticos");

            migrationBuilder.DropTable(
                name: "ContactoPaciente");

            migrationBuilder.DropTable(
                name: "CultivoInfectologico");

            migrationBuilder.DropTable(
                name: "Diagnostico");

            migrationBuilder.DropTable(
                name: "Hematologico");

            migrationBuilder.DropTable(
                name: "HisopadoInfectologico");

            migrationBuilder.DropTable(
                name: "HMNBalanceHidricoHMN");

            migrationBuilder.DropTable(
                name: "HMNGeneralHMN");

            migrationBuilder.DropTable(
                name: "HMNLaboratorioHMN");

            migrationBuilder.DropTable(
                name: "MovimientoCamaPaciente");

            migrationBuilder.DropTable(
                name: "PacienteTablaHistorica");

            migrationBuilder.DropTable(
                name: "SensibilidadCultivoInfectologico");

            migrationBuilder.DropTable(
                name: "SNCAnticonvulsionante");

            migrationBuilder.DropTable(
                name: "SNCAspectoGeneralSNC");

            migrationBuilder.DropTable(
                name: "SNCImagenesSNC");

            migrationBuilder.DropTable(
                name: "SNCLaboratorioSNC");

            migrationBuilder.DropTable(
                name: "SNCMedicacionSNC");

            migrationBuilder.DropTable(
                name: "SNCMedicamentoSedacion");

            migrationBuilder.DropTable(
                name: "SNCSxAbstinenciaSNC");

            migrationBuilder.DropTable(
                name: "EnzimasCardiacas");

            migrationBuilder.DropTable(
                name: "ApCardiovascular");

            migrationBuilder.DropTable(
                name: "Inotropicos");

            migrationBuilder.DropTable(
                name: "ParametrosSopRespiratorio");

            migrationBuilder.DropTable(
                name: "ApRespiratorio");

            migrationBuilder.DropTable(
                name: "ParametrosGasometria");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Hisopado");

            migrationBuilder.DropTable(
                name: "BalanceHidricoHMN");

            migrationBuilder.DropTable(
                name: "GeneralHMN");

            migrationBuilder.DropTable(
                name: "HMN");

            migrationBuilder.DropTable(
                name: "LaboratorioHMN");

            migrationBuilder.DropTable(
                name: "Cama");

            migrationBuilder.DropTable(
                name: "Cultivo");

            migrationBuilder.DropTable(
                name: "Infectologico");

            migrationBuilder.DropTable(
                name: "Anticonvulsionante");

            migrationBuilder.DropTable(
                name: "AspectoGeneralSNC");

            migrationBuilder.DropTable(
                name: "ImagenSNC");

            migrationBuilder.DropTable(
                name: "LaboratorioSNC");

            migrationBuilder.DropTable(
                name: "MedicacionSNC");

            migrationBuilder.DropTable(
                name: "MedicamentoSedacion");

            migrationBuilder.DropTable(
                name: "SNC");

            migrationBuilder.DropTable(
                name: "SxAbstinenciaSNC");

            migrationBuilder.DropTable(
                name: "Gasometria");

            migrationBuilder.DropTable(
                name: "Modalidad");

            migrationBuilder.DropTable(
                name: "SoporteRespiratorio");

            migrationBuilder.DropTable(
                name: "Ventilacion");

            migrationBuilder.DropTable(
                name: "CategoriaLaboratorio");

            migrationBuilder.DropTable(
                name: "EstadoInfectologico");

            migrationBuilder.DropTable(
                name: "Paciente");

            migrationBuilder.DropTable(
                name: "Sedacion");

            migrationBuilder.DropTable(
                name: "PRMS");
        }
    }
}
