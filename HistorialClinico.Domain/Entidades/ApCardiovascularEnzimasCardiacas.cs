﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class ApCardiovascularEnzimasCardiacas
    {
        public int ApCardiovascularId { get; set; }
        public int EnzimasCardiacasId { get; set; }
        public decimal? Valor { get; set; }
        public string Curva { get; set; }

        public virtual ApCardiovascular ApCardiovascular { get; set; }
        public virtual EnzimasCardiacas EnzimasCardiacas { get; set; }
    }
}
