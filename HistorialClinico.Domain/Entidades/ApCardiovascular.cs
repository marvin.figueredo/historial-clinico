﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class ApCardiovascular
    {
        public ApCardiovascular()
        {
            ApCardiovascularEnzimasCardiacas = new HashSet<ApCardiovascularEnzimasCardiacas>();
            ApCardiovascularInotropicos = new HashSet<ApCardiovascularInotropicos>();
        }

        public int Id { get; set; }
        public int PacienteId { get; set; }
        public string EstadoId { get; set; }
        public string EvaluacionCardiologica { get; set; }
        public string UserAdd { get; set; }
        public DateTime DateAdd { get; set; }
        public bool Deleted { get; set; }
        public string UserDelete { get; set; }
        public DateTime? DateDelete { get; set; }
        public string Eventos { get; set; }
        public string Planes { get; set; }

        public virtual Paciente Paciente { get; set; }
        public virtual ICollection<ApCardiovascularEnzimasCardiacas> ApCardiovascularEnzimasCardiacas { get; set; }
        public virtual ICollection<ApCardiovascularInotropicos> ApCardiovascularInotropicos { get; set; }
    }
}
