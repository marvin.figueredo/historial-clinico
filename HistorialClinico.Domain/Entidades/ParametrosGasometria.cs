﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class ParametrosGasometria
    {
        public ParametrosGasometria()
        {
            ApRespiratorioParametrosGasometria = new HashSet<ApRespiratorioParametrosGasometria>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<ApRespiratorioParametrosGasometria> ApRespiratorioParametrosGasometria { get; set; }
    }
}
