﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class SxAbstinenciaSNC
    {
        public SxAbstinenciaSNC()
        {
            SNCsxAbstinenciaSNC = new HashSet<SNCsxAbstinenciaSNC>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<SNCsxAbstinenciaSNC> SNCsxAbstinenciaSNC { get; set; }
    }
}
