﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class SNCmedicamentoSedacion
    {
        public int SNCid { get; set; }
        public int MedicamentoSedacionId { get; set; }
        public decimal Valor { get; set; }

        public virtual MedicamentoSedacion MedicamentoSedacion { get; set; }
        public virtual SNC SNC { get; set; }
    }
}
