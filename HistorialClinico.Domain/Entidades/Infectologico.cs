﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class Infectologico
    {
        public Infectologico()
        {
            CoberturaAntibioticos = new HashSet<CoberturaAntibioticos>();
            CultivoInfectologico = new HashSet<CultivoInfectologico>();
            HisopadoInfectologico = new HashSet<HisopadoInfectologico>();
            SensibilidadCultivoInfectologico = new HashSet<SensibilidadCultivoInfectologico>();
        }

        public int Id { get; set; }
        public int PacienteId { get; set; }
        public int EstadoInfectologicoId { get; set; }
        public string Interconsulta { get; set; }
        public string Planes { get; set; }
        public string UserAdd { get; set; }
        public DateTime DateAdd { get; set; }
        public bool Deleted { get; set; }
        public string UserDelete { get; set; }
        public DateTime? DateDelete { get; set; }
        public string Eventos { get; set; }

        public virtual EstadoInfectologico EstadoInfectologico { get; set; }
        public virtual Paciente Paciente { get; set; }
        public virtual ICollection<CoberturaAntibioticos> CoberturaAntibioticos { get; set; }
        public virtual ICollection<CultivoInfectologico> CultivoInfectologico { get; set; }
        public virtual ICollection<HisopadoInfectologico> HisopadoInfectologico { get; set; }
        public virtual ICollection<SensibilidadCultivoInfectologico> SensibilidadCultivoInfectologico { get; set; }
    }
}
