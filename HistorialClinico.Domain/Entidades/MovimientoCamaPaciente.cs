﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class MovimientoCamaPaciente
    {
        public int Id { get; set; }
        public int CamaId { get; set; }
        public int PacienteId { get; set; }
        public string TipoMovimientoId { get; set; }
        public DateTime Fecha { get; set; }
        public string UserName { get; set; }

        public virtual Cama Cama { get; set; }
        public virtual Paciente Paciente { get; set; }
    }
}
