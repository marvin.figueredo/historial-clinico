﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class CoberturaAntibioticos
    {
        public int Id { get; set; }
        public int InfectologicoId { get; set; }
        public string Antibiotico { get; set; }
        public decimal Dosis { get; set; }
        public string Unidad { get; set; }
        public bool AjustadoClearence { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaSuspension { get; set; }

        public virtual Infectologico Infectologico { get; set; }
    }
}
