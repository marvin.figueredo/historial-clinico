﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class AspectoGeneralSNC
    {
        public AspectoGeneralSNC()
        {
            SNCaspectoGeneralSNC = new HashSet<SNCaspectoGeneralSNC>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<SNCaspectoGeneralSNC> SNCaspectoGeneralSNC { get; set; }
    }
}
