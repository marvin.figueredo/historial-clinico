﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class Diagnostico
    {
        public int Id { get; set; }
        public int PacienteId { get; set; }
        public string Resumen { get; set; }
        public DateTime FechaAdd { get; set; }
        public string UserNameAdd { get; set; }
        public DateTime? FechaUpdate { get; set; }
        public string UserNameUpdate { get; set; }
        public DateTime? FechaDelete { get; set; }
        public string UserNameDelete { get; set; }

        public virtual Paciente Paciente { get; set; }
    }
}
