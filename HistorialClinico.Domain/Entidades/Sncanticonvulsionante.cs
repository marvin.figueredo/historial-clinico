﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class SNCanticonvulsionante
    {
        public int SNCid { get; set; }
        public int AnticonvulsionanteId { get; set; }
        public decimal Valor { get; set; }

        public virtual Anticonvulsionante Anticonvulsionante { get; set; }
        public virtual SNC SNC { get; set; }
    }
}
