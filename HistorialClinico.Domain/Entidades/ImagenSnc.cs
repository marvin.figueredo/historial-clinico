﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class ImagenSNC
    {
        public ImagenSNC()
        {
            SNCimagenesSNC = new HashSet<SNCimagenesSNC>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<SNCimagenesSNC> SNCimagenesSNC { get; set; }
    }
}
