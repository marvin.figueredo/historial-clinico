﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class Anticonvulsionante
    {
        public Anticonvulsionante()
        {
            SNCanticonvulsionante = new HashSet<SNCanticonvulsionante>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<SNCanticonvulsionante> SNCanticonvulsionante { get; set; }
    }
}
