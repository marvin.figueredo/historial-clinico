﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class LaboratorioSNC
    {
        public LaboratorioSNC()
        {
            SNClaboratorioSNC = new HashSet<SNClaboratorioSNC>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<SNClaboratorioSNC> SNClaboratorioSNC { get; set; }
    }
}
