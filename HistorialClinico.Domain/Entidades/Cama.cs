﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class Cama
    {
        public Cama()
        {
            MovimientoCamaPaciente = new HashSet<MovimientoCamaPaciente>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual CamaPaciente CamaPaciente { get; set; }
        public virtual ICollection<MovimientoCamaPaciente> MovimientoCamaPaciente { get; set; }
    }
}
