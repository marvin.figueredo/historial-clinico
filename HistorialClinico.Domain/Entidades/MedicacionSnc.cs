﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class MedicacionSNC
    {
        public MedicacionSNC()
        {
            SNCmedicacionSNC = new HashSet<SNCmedicacionSNC>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<SNCmedicacionSNC> SNCmedicacionSNC { get; set; }
    }
}
