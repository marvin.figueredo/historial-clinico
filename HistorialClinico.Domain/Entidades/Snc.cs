﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class SNC
    {
        public SNC()
        {
            SNCanticonvulsionante = new HashSet<SNCanticonvulsionante>();
            SNCaspectoGeneralSNC = new HashSet<SNCaspectoGeneralSNC>();
            SNCimagenesSNC = new HashSet<SNCimagenesSNC>();
            SNClaboratorioSNC = new HashSet<SNClaboratorioSNC>();
            SNCmedicacionSNC = new HashSet<SNCmedicacionSNC>();
            SNCmedicamentoSedacion = new HashSet<SNCmedicamentoSedacion>();
            SNCsxAbstinenciaSNC = new HashSet<SNCsxAbstinenciaSNC>();
        }

        public int Id { get; set; }
        public int PacienteId { get; set; }
        public int? SedacionId { get; set; }
        public decimal? ValorSedacion { get; set; }
        public bool SxAbstinencia { get; set; }
        public bool ConocidoConvulsionador { get; set; }
        public string UserAdd { get; set; }
        public DateTime DateAdd { get; set; }
        public bool Deleted { get; set; }
        public string UserDelete { get; set; }
        public DateTime? DateDelete { get; set; }
        public string Eventos { get; set; }
        public string Planes { get; set; }

        public virtual Paciente Paciente { get; set; }
        public virtual Sedacion Sedacion { get; set; }
        public virtual ICollection<SNCanticonvulsionante> SNCanticonvulsionante { get; set; }
        public virtual ICollection<SNCaspectoGeneralSNC> SNCaspectoGeneralSNC { get; set; }
        public virtual ICollection<SNCimagenesSNC> SNCimagenesSNC { get; set; }
        public virtual ICollection<SNClaboratorioSNC> SNClaboratorioSNC { get; set; }
        public virtual ICollection<SNCmedicacionSNC> SNCmedicacionSNC { get; set; }
        public virtual ICollection<SNCmedicamentoSedacion> SNCmedicamentoSedacion { get; set; }
        public virtual ICollection<SNCsxAbstinenciaSNC> SNCsxAbstinenciaSNC { get; set; }
    }
}
