﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class SoporteRespiratorio
    {
        public SoporteRespiratorio()
        {
            ApRespiratorio = new HashSet<ApRespiratorio>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool Parametros { get; set; }

        public virtual ICollection<ApRespiratorio> ApRespiratorio { get; set; }
    }
}
