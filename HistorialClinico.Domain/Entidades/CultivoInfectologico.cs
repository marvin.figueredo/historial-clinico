﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class CultivoInfectologico
    {
        public int InfectologicoId { get; set; }
        public int CultivoId { get; set; }
        public bool Resultado { get; set; }
        public DateTime Fecha { get; set; }

        public virtual Cultivo Cultivo { get; set; }
        public virtual Infectologico Infectologico { get; set; }
    }
}
