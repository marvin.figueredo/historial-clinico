﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class ApRespiratorioParametrosGasometria
    {
        public int ApRespiratorioId { get; set; }
        public int ParametroId { get; set; }
        public decimal Valor { get; set; }

        public virtual ApRespiratorio ApRespiratorio { get; set; }
        public virtual ParametrosGasometria Parametro { get; set; }
    }
}
