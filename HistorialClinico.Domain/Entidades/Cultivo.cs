﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class Cultivo
    {
        public Cultivo()
        {
            CultivoInfectologico = new HashSet<CultivoInfectologico>();
            SensibilidadCultivoInfectologico = new HashSet<SensibilidadCultivoInfectologico>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<CultivoInfectologico> CultivoInfectologico { get; set; }
        public virtual ICollection<SensibilidadCultivoInfectologico> SensibilidadCultivoInfectologico { get; set; }
    }
}
