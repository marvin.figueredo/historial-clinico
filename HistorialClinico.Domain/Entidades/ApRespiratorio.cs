﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class ApRespiratorio
    {
        public ApRespiratorio()
        {
            ApRespiratorioParametros = new HashSet<ApRespiratorioParametros>();
            ApRespiratorioParametrosGasometria = new HashSet<ApRespiratorioParametrosGasometria>();
        }

        public int Id { get; set; }
        public int PacienteId { get; set; }
        public int? SoporteRespiratorioId { get; set; }
        public string ValorSoporteResp { get; set; }
        public int? VentilacionId { get; set; }
        public int? ModalidadId { get; set; }
        public int? GasometriaId { get; set; }
        public string Manejo { get; set; }
        public string UserAdd { get; set; }
        public DateTime DateAdd { get; set; }
        public bool Deleted { get; set; }
        public string UserDelete { get; set; }
        public DateTime? DateDelete { get; set; }
        public string Eventos { get; set; }
        public string Planes { get; set; }

        public virtual Gasometria Gasometria { get; set; }
        public virtual Modalidad Modalidad { get; set; }
        public virtual Paciente Paciente { get; set; }
        public virtual SoporteRespiratorio SoporteRespiratorio { get; set; }
        public virtual Ventilacion Ventilacion { get; set; }
        public virtual ICollection<ApRespiratorioParametros> ApRespiratorioParametros { get; set; }
        public virtual ICollection<ApRespiratorioParametrosGasometria> ApRespiratorioParametrosGasometria { get; set; }
    }
}
