﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class HMNLaboratorioHMN
    {
        public int HMNid { get; set; }
        public int LaboratorioHMNid { get; set; }
        public decimal? Valor { get; set; }

        public virtual HMN HMN { get; set; }
        public virtual LaboratorioHMN LaboratorioHMN { get; set; }
    }
}
