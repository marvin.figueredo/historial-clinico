﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class SNCimagenesSNC
    {
        public int SNCid { get; set; }
        public int ImagenesSNCid { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Valor { get; set; }

        public virtual ImagenSNC ImagenesSNC { get; set; }
        public virtual SNC SNC { get; set; }
    }
}
