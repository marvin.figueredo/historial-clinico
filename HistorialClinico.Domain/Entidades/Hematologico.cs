﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class Hematologico
    {
        public int Id { get; set; }
        public int PacienteId { get; set; }
        public decimal? HemogramaPai { get; set; }
        public decimal? HemogramaHb { get; set; }
        public decimal? HemogramaHtc { get; set; }
        public decimal? HemogramaPlt { get; set; }
        public decimal? CrasisTp { get; set; }
        public decimal? CrasisTtpa { get; set; }
        public decimal? CrasisFibrinoginos { get; set; }
        public bool VitaminaK { get; set; }
        public decimal? DosisVitaminaK { get; set; }
        public DateTime? FechaVitaminaK { get; set; }
        public bool SangradoActivo { get; set; }
        public string LugarSangrado { get; set; }
        public bool Transfusiones { get; set; }
        public decimal? TransfusionesGrc { get; set; }
        public decimal? TransfusionesPfc { get; set; }
        public decimal? TransfusionesPlt { get; set; }
        public decimal? TransfusionesCrio { get; set; }
        public string Eventos { get; set; }
        public string Planes { get; set; }
        public string UserAdd { get; set; }
        public DateTime DateAdd { get; set; }
        public bool Deleted { get; set; }
        public string UserDelete { get; set; }
        public DateTime? DateDelete { get; set; }
    }
}
