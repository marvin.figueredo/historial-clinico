﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class Hisopado
    {
        public Hisopado()
        {
            HisopadoInfectologico = new HashSet<HisopadoInfectologico>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<HisopadoInfectologico> HisopadoInfectologico { get; set; }
    }
}
