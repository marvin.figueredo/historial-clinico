﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class MedicamentoSedacion
    {
        public MedicamentoSedacion()
        {
            SNCmedicamentoSedacion = new HashSet<SNCmedicamentoSedacion>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<SNCmedicamentoSedacion> SNCmedicamentoSedacion { get; set; }
    }
}
