﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class HMN
    {
        public HMN()
        {
            HMNbalanceHidricoHMN = new HashSet<HMNBalanceHidricoHMN>();
            HMNgeneralHMN = new HashSet<HMNGeneralHMN>();
            HMNlaboratorioHMN = new HashSet<HMNLaboratorioHMN>();
        }

        public int Id { get; set; }
        public int PacienteId { get; set; }
        public bool DialisisPeritoneal { get; set; }
        public string FormulacionDialisisPeritoneal { get; set; }
        public string Eventos { get; set; }
        public string Planes { get; set; }
        public string UserAdd { get; set; }
        public DateTime DateAdd { get; set; }
        public bool Deleted { get; set; }
        public string UserDelete { get; set; }
        public DateTime? DateDelete { get; set; }

        public virtual Paciente Paciente { get; set; }
        public virtual ICollection<HMNBalanceHidricoHMN> HMNbalanceHidricoHMN { get; set; }
        public virtual ICollection<HMNGeneralHMN> HMNgeneralHMN { get; set; }
        public virtual ICollection<HMNLaboratorioHMN> HMNlaboratorioHMN { get; set; }
    }
}
