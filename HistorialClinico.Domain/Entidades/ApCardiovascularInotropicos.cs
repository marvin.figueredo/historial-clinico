﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class ApCardiovascularInotropicos
    {
        public int ApCardiovascularId { get; set; }
        public int InotropicosId { get; set; }
        public decimal Valor { get; set; }

        public virtual ApCardiovascular ApCardiovascular { get; set; }
        public virtual Inotropico Inotropicos { get; set; }
    }
}
