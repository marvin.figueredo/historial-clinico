﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class BalanceHidricoHMN
    {
        public BalanceHidricoHMN()
        {
            HMNbalanceHidricoHMN = new HashSet<HMNBalanceHidricoHMN>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<HMNBalanceHidricoHMN> HMNbalanceHidricoHMN { get; set; }
    }
}
