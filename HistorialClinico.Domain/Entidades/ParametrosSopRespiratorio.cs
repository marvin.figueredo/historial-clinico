﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class ParametrosSopRespiratorio
    {
        public ParametrosSopRespiratorio()
        {
            ApRespiratorioParametros = new HashSet<ApRespiratorioParametros>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Orden { get; set; }

        public virtual ICollection<ApRespiratorioParametros> ApRespiratorioParametros { get; set; }
    }
}
