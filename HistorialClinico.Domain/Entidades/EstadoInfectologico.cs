﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class EstadoInfectologico
    {
        public EstadoInfectologico()
        {
            Infectologico = new HashSet<Infectologico>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Infectologico> Infectologico { get; set; }
    }
}
