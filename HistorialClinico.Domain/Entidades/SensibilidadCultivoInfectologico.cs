﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class SensibilidadCultivoInfectologico
    {
        public int Id { get; set; }
        public int? InfectologicoId { get; set; }
        public int? CultivoId { get; set; }
        public string Sensibilidad { get; set; }

        public virtual Cultivo Cultivo { get; set; }
        public virtual Infectologico Infectologico { get; set; }
    }
}
