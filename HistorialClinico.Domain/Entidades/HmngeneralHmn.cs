﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class HMNGeneralHMN
    {
        public int HMNid { get; set; }
        public int GeneralHMNid { get; set; }
        public decimal? Valor { get; set; }
        public string Formulacion { get; set; }

        public virtual GeneralHMN GeneralHMN { get; set; }
        public virtual HMN HMN { get; set; }
    }
}
