﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class Sedacion
    {
        public Sedacion()
        {
            SNC = new HashSet<SNC>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool RequeridoValor { get; set; }

        public virtual ICollection<SNC> SNC { get; set; }
    }
}
