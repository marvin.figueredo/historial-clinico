﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class HisopadoInfectologico
    {
        public int InfectologicoId { get; set; }
        public int HisopadoId { get; set; }
        public bool Resultado { get; set; }
        public DateTime Fecha { get; set; }

        public virtual Hisopado Hisopado { get; set; }
        public virtual Infectologico Infectologico { get; set; }
    }
}
