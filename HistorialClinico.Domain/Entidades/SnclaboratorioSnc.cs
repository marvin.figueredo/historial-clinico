﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class SNClaboratorioSNC
    {
        public int SNCid { get; set; }
        public int LaboratorioSNCid { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Valor { get; set; }

        public virtual LaboratorioSNC LaboratorioSNC { get; set; }
        public virtual SNC SNC { get; set; }
    }
}
