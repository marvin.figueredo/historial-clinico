﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class PacienteTablaHistorica
    {
        public int Id { get; set; }
        public int PacienteId { get; set; }
        public string NroDocumento { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Sexo { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string GrupoSanguineo { get; set; }
        public decimal? Peso { get; set; }
        public DateTime FechaAdd { get; set; }
        public string UserNameAdd { get; set; }
        public int? PrmsId { get; set; }

        public virtual Paciente Paciente { get; set; }
        public virtual PRMS PRMS { get; set; }
    }
}
