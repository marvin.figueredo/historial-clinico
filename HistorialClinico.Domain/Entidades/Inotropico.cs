﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class Inotropico
    {
        public Inotropico()
        {
            ApCardiovascularInotropicos = new HashSet<ApCardiovascularInotropicos>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<ApCardiovascularInotropicos> ApCardiovascularInotropicos { get; set; }
    }
}
