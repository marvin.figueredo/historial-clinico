﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class HMNBalanceHidricoHMN
    {
        public int HMNid { get; set; }
        public int BalanceHidricoHMNid { get; set; }
        public decimal? Valor { get; set; }

        public virtual BalanceHidricoHMN BalanceHidricoHMN { get; set; }
        public virtual HMN HMN { get; set; }
    }
}
