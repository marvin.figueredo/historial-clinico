﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class GeneralHMN
    {
        public GeneralHMN()
        {
            HMNgeneralHMN = new HashSet<HMNGeneralHMN>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public bool Valor { get; set; }
        public bool Formulacion { get; set; }

        public virtual ICollection<HMNGeneralHMN> HMNgeneralHMN { get; set; }
    }
}
