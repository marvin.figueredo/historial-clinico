﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class ApRespiratorioParametros
    {
        public int ApRespiratorioId { get; set; }
        public int ParametroId { get; set; }
        public decimal Valor { get; set; }

        public virtual ApRespiratorio ApRespiratorio { get; set; }
        public virtual ParametrosSopRespiratorio Parametro { get; set; }
    }
}
