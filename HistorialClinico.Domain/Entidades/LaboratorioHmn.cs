﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class LaboratorioHMN
    {
        public LaboratorioHMN()
        {
            HMNlaboratorioHMN = new HashSet<HMNLaboratorioHMN>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int CategoriaId { get; set; }

        public virtual CategoriaLaboratorio Categoria { get; set; }
        public virtual ICollection<HMNLaboratorioHMN> HMNlaboratorioHMN { get; set; }
    }
}
