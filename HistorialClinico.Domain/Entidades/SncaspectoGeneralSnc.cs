﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class SNCaspectoGeneralSNC
    {
        public int SNCid { get; set; }
        public int AspectoGeneralId { get; set; }

        public virtual AspectoGeneralSNC AspectoGeneral { get; set; }
        public virtual SNC SNC { get; set; }
    }
}
