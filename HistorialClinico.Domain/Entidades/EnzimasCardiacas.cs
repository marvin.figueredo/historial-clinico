﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class EnzimasCardiacas
    {
        public EnzimasCardiacas()
        {
            ApCardiovascularEnzimasCardiacas = new HashSet<ApCardiovascularEnzimasCardiacas>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<ApCardiovascularEnzimasCardiacas> ApCardiovascularEnzimasCardiacas { get; set; }
    }
}
