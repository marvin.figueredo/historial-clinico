﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class CamaPaciente
    {
        public int CamaId { get; set; }
        public int PacienteId { get; set; }
        public DateTime FechaIngreso { get; set; }

        public virtual Cama Cama { get; set; }
        public virtual Paciente Paciente { get; set; }
    }
}
