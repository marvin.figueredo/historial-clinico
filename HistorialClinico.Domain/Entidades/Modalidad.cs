﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class Modalidad
    {
        public Modalidad()
        {
            ApRespiratorio = new HashSet<ApRespiratorio>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<ApRespiratorio> ApRespiratorio { get; set; }
    }
}
