﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class SNCsxAbstinenciaSNC
    {
        public int SNCid { get; set; }
        public int SxAbstinenciaSNCid { get; set; }
        public decimal Valor { get; set; }

        public virtual SNC SNC { get; set; }
        public virtual SxAbstinenciaSNC SxAbstinenciaSNC { get; set; }
    }
}
