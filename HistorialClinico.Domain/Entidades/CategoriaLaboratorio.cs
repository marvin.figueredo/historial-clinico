﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class CategoriaLaboratorio
    {
        public CategoriaLaboratorio()
        {
            LaboratorioHMN = new HashSet<LaboratorioHMN>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<LaboratorioHMN> LaboratorioHMN { get; set; }
    }
}
