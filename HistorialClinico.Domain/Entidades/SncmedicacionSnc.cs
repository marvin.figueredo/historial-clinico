﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class SNCmedicacionSNC
    {
        public int SNCid { get; set; }
        public int MedicacionSNCid { get; set; }
        public decimal Valor { get; set; }

        public virtual MedicacionSNC MedicacionSNC { get; set; }
        public virtual SNC SNC { get; set; }
    }
}
