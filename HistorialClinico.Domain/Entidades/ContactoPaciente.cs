﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain
{
    public partial class ContactoPaciente
    {
        public int Id { get; set; }
        public int PacienteId { get; set; }
        public string NombreContacto { get; set; }
        public string NroContacto { get; set; }
        public string TipoContactoId { get; set; }
        public DateTime FechaAdd { get; set; }
        public string UserNameAdd { get; set; }
        public DateTime? FechaUpdate { get; set; }
        public string UserNameUpdate { get; set; }
        public DateTime? FechaDelete { get; set; }
        public string UserNameDelete { get; set; }

        public virtual Paciente Paciente { get; set; }
    }
}
