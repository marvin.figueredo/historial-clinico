﻿using System;

namespace HistorialClinico.Domain.DTO
{
    public partial class CamaPacienteDTO
    {
        public int Id { get; set; }
        public string Cama { get; set; }
        public int? PacienteId { get; set; }
        public string NombrePaciente { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public DateTime? FechaIngreso { get; set; }

    }
}