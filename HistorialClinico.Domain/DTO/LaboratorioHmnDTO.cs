﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain.DTO
{
    public partial class LaboratorioHMNDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int CategoriaId { get; set; }
        public string Categoria { get; set; }

    }
}
