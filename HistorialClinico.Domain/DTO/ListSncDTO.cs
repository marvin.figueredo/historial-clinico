﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HistorialClinico.Domain.DTO
{
    public class ListSNCDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Valor { get; set; }
    }
}
