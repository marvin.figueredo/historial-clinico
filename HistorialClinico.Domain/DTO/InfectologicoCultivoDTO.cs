﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HistorialClinico.Domain.DTO
{
    public class InfectologicoCultivoDTO
    {
        public int CultivoId { get; set; }
        public string Cultivo { get; set; }
        public bool Resultado { get; set; }
        public DateTime Fecha { get; set; }
    }
}