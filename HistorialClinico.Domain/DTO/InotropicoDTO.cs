﻿namespace HistorialClinico.Domain.DTO
{
    public class InotropicoDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Valor { get; set; }
    }
}