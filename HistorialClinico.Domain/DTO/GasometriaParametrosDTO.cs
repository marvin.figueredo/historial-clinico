﻿namespace HistorialClinico.Domain.DTO
{
    public class GasometriaParametrosDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public decimal Valor { get; set; }
    }
}
