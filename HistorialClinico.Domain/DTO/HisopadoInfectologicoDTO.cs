﻿using System;
using System.Collections.Generic;

namespace HistorialClinico.Domain.DTO
{
    public partial class HisopadoInfectologicoDTO
    {
        public int InfectologicoId { get; set; }
        public int HisopadoId { get; set; }
        public string Hisopado { get; set; }
        public bool Resultado { get; set; }
        public DateTime Fecha { get; set; }

    }
}
