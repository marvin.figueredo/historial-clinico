#region Using

using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using HistorialClinico.Common.Configuration;
using HistorialClinico.Domain;
using HistorialClinico.Infrastructure;
using Microsoft.Extensions.Logging;
using HistorialClinico.Web.Middleware;
using HistorialClinico.Services;
using HistorialClinico.Services.Interfaces;
using AutoMapper;
using HistorialClinico.Domain.DTO;
using HistorialClinico.Web.Models.Paciente;
using System.Globalization;
using Microsoft.Extensions.Hosting;

#endregion

namespace HistorialClinico.Web
{
    /// <summary>
    /// Defines the startup instance used by the web host.
    /// </summary>
    [UsedImplicitly]
    public class Startup
    {
        private IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            // Expose the injected instance locally so we populate our settings instance
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.AddConfiguration(_configuration.GetSection("Logging"));
                loggingBuilder.AddFilter("Microsoft", LogLevel.None);
                loggingBuilder.AddFilter("DbCommand ", LogLevel.None);
                loggingBuilder.AddFilter("Request ", LogLevel.None);
                loggingBuilder.AddFilter("Identity", LogLevel.None);
                loggingBuilder.AddFilter("Authorization", LogLevel.None);
                loggingBuilder.AddFilter("Executing", LogLevel.None);
                loggingBuilder.AddFilter("Executed", LogLevel.None);
                loggingBuilder.AddConsole();
            });

            services.AddAutoMapper(cfg =>
            {
                cfg.CreateMap<Paciente, PacienteDTO>();
                cfg.CreateMap<ContactoPaciente, ContactoPacienteDTO>();
                cfg.CreateMap<Diagnostico, DiagnosticoDTO>()
                        .ForMember(d => d.Fecha, o => o.MapFrom(s => s.FechaAdd))
                        .ForMember(d => d.UserName, o => o.MapFrom(s => s.UserNameAdd));
                cfg.CreateMap<ApCardiovascular, ApCardiovascularDTO>();
                cfg.CreateMap<ApRespiratorioFormModel, ApRespiratorioDTO>();
                cfg.CreateMap<Hematologico, HematologicoFormModel>();
                cfg.CreateMap<Cirugia, CirugiaDTO>();
            }, typeof(Startup));

            // Bind the settings instance as a singleton and expose it as an options type (IOptions<AppSettings>)
            // Note: This ensures that injecting both IOptions<T> and T is made possible and will resolve
            services.Configure<AppSettings>(_configuration);

            // Bind the settings instance as a singleton and expose it as an options type (IOptions<SmartSettings>)
            services.Configure<SmartSettings>(_configuration.GetSection("SmartAdmin"));

            // We retrieve the current bound AppSettings instance in order to access the connection string
            // Note: While this does performs a model binding to the type, it does not modify the service collection
            var settings = _configuration.Get<AppSettings>();

            // We need essential Mvc services and DI support to host the template pages
            services.AddMvc().AddControllersAsServices();

            // We allow our routes to be in lowercase
            services.AddRouting(options => options.LowercaseUrls = true);

            // We will setup this simple seeding helper to ensure default data is present
            services.AddTransient<ApplicationDbSeeder>();

            // Add the default identity classes and schema for use with EntityFramework
            services.AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();

            services.AddDbContext<ApplicationDbContext>(options => options.UseNpgsql(settings.ConnectionString));

            // Add application services.
            services.Configure<EmailSettings>(_configuration.GetSection("EmailSettings"));
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IFichaPacienteService, FichaPacienteService>();
            services.AddTransient<ICamaService, CamaService>();
            services.AddTransient<IApCardiovascularService, ApCardiovascularService>();
            services.AddTransient<IApRespiratorioService, ApRespiratorioService>();
            services.AddTransient<IInfectologicoService, InfectologicoService>();
            services.AddTransient<ISNCService, SNCService>();
            services.AddTransient<IHMNService, HMNService>();
            services.AddTransient<IHematologicoService, HematologicoService>();
            services.AddTransient<ICirugiaService, CirugiaService>();

            // Cache 200 (OK) server responses; any other responses, including error pages, are ignored.
            services.AddResponseCaching();

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ApplicationDbSeeder dbSeeder, ILoggerFactory loggerFactory)
        {
            var cultureInfo = new CultureInfo("es-ES");

            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;

            loggerFactory.AddFile("Logs/HistorialClinico-{Date}.txt");

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            // Warning: Do not trigger this seed in your production environment, this is a security risk!
            if (!env.IsProduction())
                // Ensure we have the default user added to the store
                dbSeeder.EnsureSeed().GetAwaiter().GetResult();

            // Enable the authentication middleware so we can protect access to controllers and/or actions
            app.UseAuthentication();

            // Enable the reponse caching middleware to serve 200 OK responses directly from cache on sub-sequent requests
            app.UseResponseCaching();

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
