#/bin/bash
sudo dotnet clean --configuration Release
sudo dotnet publish -c Release
sudo cp Dockerfile ./bin/Release/netcoreapp3.1/publish/
cd ./bin/Release/netcoreapp3.1/publish/
sudo rm appsettings.Development.json
sudo heroku container:login
sudo heroku container:push web -a fichaterapia
sudo heroku container:release web -a fichaterapia