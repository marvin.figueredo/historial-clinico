﻿using AutoMapper;
using HistorialClinico.Domain;
using HistorialClinico.Domain.DTO;
using HistorialClinico.Infrastructure;
using HistorialClinico.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace HistorialClinico.Services
{
    public class ApCardiovascularService : SqlHelperService, IApCardiovascularService
    {
        #region Inicio
        private readonly ApplicationDbContext _dbContext;
        private readonly IMapper mapper;
        private readonly string _connectionString;

        public ApCardiovascularService(ApplicationDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            this.mapper = mapper;
            _connectionString = _dbContext.Database.GetDbConnection().ConnectionString;
        }
        #endregion

        #region Métodos públicos

        public async Task<List<ApCardiovascularDTO>> ListApCardiovascularAsync(int PacienteId)
        {
            var items = await _dbContext.ApCardiovascular.Where(c => c.PacienteId == PacienteId && !c.Deleted)
               .OrderByDescending(c => c.Id)
               .ToListAsync();

            return mapper.Map<List<ApCardiovascularDTO>>(items);
        }

        public async Task<List<InotropicoDTO>> ListInotropicosAsync(int ApCardiovascularId, bool Relacionados)
        {
            if (Relacionados)
            {
                var inotropicos = await _dbContext.ApCardiovascularInotropicos.Where(c => c.ApCardiovascularId == ApCardiovascularId)
                    .Select(c => new InotropicoDTO()
                    {
                        Id = c.InotropicosId,
                        Nombre = c.Inotropicos.Nombre,
                        Valor = c.Valor.ToString("N2")
                    }).ToListAsync();

                return inotropicos;
            }
            else
            {
                var list_ids = await _dbContext.ApCardiovascularInotropicos.Where(c => c.ApCardiovascularId == ApCardiovascularId)
                        .Select(c => c.InotropicosId)
                        .ToListAsync();

                var inotropicos = await _dbContext.Inotropicos.Where(c => !list_ids.Contains(c.Id))
                    .Select(c => new InotropicoDTO()
                    {
                        Id = c.Id,
                        Nombre = c.Nombre,
                        Valor = "0"
                    }).ToListAsync();

                return inotropicos;
            }
        }

        public async Task<List<EnzimaCardiacaDTO>> ListEnzimasCardiacasAsync(int ApCardiovascularId)
        {
            List<EnzimaCardiacaDTO> model = new List<EnzimaCardiacaDTO>();

            var items = await _dbContext.EnzimasCardiacas.ToListAsync();

            foreach (var item in items)
            {
                var item_asociado = await _dbContext.ApCardiovascularEnzimasCardiacas.FindAsync(ApCardiovascularId, item.Id);

                model.Add(new EnzimaCardiacaDTO()
                {
                    Id = item.Id,
                    Nombre = item.Nombre,
                    Valor = item_asociado == null ? "" : (item_asociado.Valor ?? 0).ToString("N2"),
                    Curva = item_asociado == null ? "" : item_asociado.Curva
                });
            }

            return model;
        }

        public async Task AddApCardiovascularAsync(int PacienteId, string EstadoId, string EvaluacionCardiologica, string InotropicosJSON, 
            string EnzimasCardiacasJSON, string Eventos, string Planes, string user_name)
        {
            var item = new ApCardiovascular()
            {
                PacienteId = PacienteId,
                EstadoId = EstadoId,
                EvaluacionCardiologica = EvaluacionCardiologica,
                Eventos = Eventos,
                Planes = Planes,
                UserAdd = user_name,
                DateAdd = DateTime.Now,
                Deleted = false
            };

            if (!string.IsNullOrWhiteSpace(InotropicosJSON))
            {
                var inotropicos = JsonConvert.DeserializeObject<List<InotropicoDTO>>(InotropicosJSON);

                item.ApCardiovascularInotropicos = inotropicos.Select(c => new ApCardiovascularInotropicos()
                {
                    InotropicosId = c.Id,
                    Valor = decimal.Parse(string.IsNullOrWhiteSpace(c.Valor) ? "0" : c.Valor)
                }).ToList();
            }

            if (!string.IsNullOrWhiteSpace(EnzimasCardiacasJSON))
            {
                var enzimas = JsonConvert.DeserializeObject<List<EnzimaCardiacaDTO>>(EnzimasCardiacasJSON);

                item.ApCardiovascularEnzimasCardiacas = enzimas.Select(c => new ApCardiovascularEnzimasCardiacas()
                {
                    EnzimasCardiacasId = c.Id,
                    Valor = decimal.Parse(string.IsNullOrWhiteSpace(c.Valor) ? "0" : c.Valor),
                    Curva = c.Curva
                }).ToList();
            }

            _dbContext.ApCardiovascular.Add(item);

            await _dbContext.SaveChangesAsync();
        }

        #endregion
    }
}