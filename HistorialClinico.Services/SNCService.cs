﻿using HistorialClinico.Domain.DTO;
using HistorialClinico.Domain;
using HistorialClinico.Infrastructure;
using HistorialClinico.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using Npgsql;
using System.Globalization;
using System.Threading.Tasks;
using System.Linq;

namespace HistorialClinico.Services
{
    public class SNCService : SqlHelperService, ISNCService
    {
        #region Inicio
        private readonly ApplicationDbContext _dbContext;
        private readonly string _connectionString;
        private readonly CultureInfo ci;

        public SNCService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _connectionString = _dbContext.Database.GetDbConnection().ConnectionString;
            ci = CultureInfo.GetCultureInfo("es-ES");
        }
        #endregion

        #region Metodos publicos
        public async Task<List<AspectoGeneralSNC>> ListAspectoGralAsync()
        {
            return await _dbContext.AspectoGeneralSNC.ToListAsync();
        }

        public async Task<List<Sedacion>> ListSedacionAsync()
        {
            return await _dbContext.Sedacion.ToListAsync();
        }

        public async Task<List<MedicamentoSedacion>> ListMedicamentoSedacionAsync()
        {
            return await _dbContext.MedicamentoSedacion.ToListAsync();
        }

        public async Task<List<LaboratorioSNC>> ListLaboratorioSNCAsync()
        {
            return await _dbContext.LaboratorioSNC.ToListAsync();
        }

        public async Task<List<ImagenSNC>> ListImagenSNCAsync()
        {
            return await _dbContext.ImagenSNC.ToListAsync();
        }

        public async Task<List<SxAbstinenciaSNC>> ListSxAbstinenciaSNCAsync()
        {
            return await _dbContext.SxAbstinenciaSNC.ToListAsync();
        }

        public async Task<List<MedicacionSNC>> ListMedicacionSNCAsync()
        {
            return await _dbContext.MedicacionSNC.ToListAsync();
        }

        public async Task<List<Anticonvulsionante>> ListAnticonvulsionanteAsync()
        {
            return await _dbContext.Anticonvulsionante.ToListAsync();
        }

        public async Task<List<SNCDTO>> ListSNCAsync(int PacienteId)
        {
            return await _dbContext.SNC.Where(c => c.PacienteId == PacienteId && !c.Deleted)
                .OrderByDescending(c => c.Id)
                .Select(c => new SNCDTO()
                {
                    Id = c.Id,
                    PacienteId = c.PacienteId,
                    SedacionId = (c.SedacionId ?? 0),
                    Sedacion = !c.SedacionId.HasValue ? "" : c.Sedacion.Nombre,
                    ValorSedacion = (c.ValorSedacion ?? 0),
                    SxAbstinenciaId = c.SxAbstinencia,
                    ConocidoConvulsionadorId = c.ConocidoConvulsionador,
                    Eventos = c.Eventos,
                    Planes = c.Planes,
                    UserName = c.UserAdd,
                    DateAdd = c.DateAdd
                }).ToListAsync();
        }

        public async Task<List<ListSNCDTO>> SNCAspectoGralAsync(int SNCId)
        {
            return await _dbContext.SNCaspectoGeneralSNC.Where(c => c.SNCid == SNCId)
                .Select(c => new ListSNCDTO()
                {
                    Id = c.AspectoGeneralId,
                    Nombre = c.AspectoGeneral.Nombre
                }).ToListAsync();
        }
               
        public async Task<List<ListSNCDTO>> SNCMedicamentoSedacionAsync(int SNCId)
        {
            return await _dbContext.SNCmedicamentoSedacion.Where(c => c.SNCid == SNCId)
                .Select(c => new ListSNCDTO()
                {
                    Id = c.MedicamentoSedacionId,
                    Nombre = c.MedicamentoSedacion.Nombre,
                    Valor = c.Valor
                }).ToListAsync();
        }

        public async Task<List<ListSNCDTO>> SNCLaboratorioAsync(int SNCId)
        {
            return await _dbContext.SNClaboratorioSNC.Where(c => c.SNCid == SNCId)
                .Select(c => new ListSNCDTO()
                {
                    Id = c.LaboratorioSNCid,
                    Nombre = c.LaboratorioSNC.Nombre,
                    Valor = c.Valor,
                    Fecha = c.Fecha
                }).ToListAsync();
        }

        public async Task<List<ListSNCDTO>> SNCImagenesAsync(int SNCId)
        {
            return await _dbContext.SNCimagenesSNC.Where(c => c.SNCid == SNCId)
                .Select(c => new ListSNCDTO()
                {
                    Id = c.ImagenesSNCid,
                    Nombre = c.ImagenesSNC.Nombre,
                    Valor = c.Valor,
                    Fecha = c.Fecha
                }).ToListAsync();
        }

        public async Task<List<ListSNCDTO>> SNCSxAbstinenciaAsync(int SNCId)
        {
            return await _dbContext.SNCsxAbstinenciaSNC.Where(c => c.SNCid == SNCId)
                 .Select(c => new ListSNCDTO()
                 {
                     Id = c.SxAbstinenciaSNCid,
                     Nombre = c.SxAbstinenciaSNC.Nombre,
                     Valor = c.Valor
                 }).ToListAsync();
        }

        public async Task<List<ListSNCDTO>> SNCAbstinenciaMedicacionAsync(int SNCId)
        {
            return await _dbContext.SNCmedicacionSNC.Where(c => c.SNCid == SNCId)
                .Select(c => new ListSNCDTO()
                {
                    Id = c.MedicacionSNCid,
                    Nombre = c.MedicacionSNC.Nombre,
                    Valor = c.Valor
                }).ToListAsync();
        }

        public async Task<List<ListSNCDTO>> SNCAnticonvulsionanteAsync(int SNCId)
        {
            return await _dbContext.SNCanticonvulsionante.Where(c => c.SNCid == SNCId)
                .Select(c => new ListSNCDTO()
                {
                    Id = c.AnticonvulsionanteId,
                    Nombre = c.Anticonvulsionante.Nombre,
                    Valor = c.Valor
                }).ToListAsync();
        }

        public async Task AddSNCAsync(SNCDTO model)
        {
            var item = new SNC()
            {
                PacienteId = model.PacienteId,
                SedacionId = model.SedacionId,
                ValorSedacion = model.ValorSedacion,
                SxAbstinencia = model.SxAbstinenciaId,
                ConocidoConvulsionador = model.ConocidoConvulsionadorId,
                Eventos = model.Eventos,
                Planes = model.Planes,
                UserAdd = model.UserName,
                DateAdd = DateTime.Now,
                Deleted = false
            };

            if (!string.IsNullOrWhiteSpace(model.AspectoGralJSON))
            {
                var asp_gral = JsonConvert.DeserializeObject<List<SNSListasDTO>>(model.AspectoGralJSON);

                item.SNCaspectoGeneralSNC = asp_gral.Select(c => new SNCaspectoGeneralSNC()
                {
                    AspectoGeneralId = c.Id
                }).ToList();
            }

            if (!string.IsNullOrWhiteSpace(model.SedacionMedicamentoJSON))
            {
                var sedacion = JsonConvert.DeserializeObject<List<SNSListasDTO>>(model.SedacionMedicamentoJSON);

                item.SNCmedicamentoSedacion = sedacion.Select(c => new SNCmedicamentoSedacion()
                {
                    MedicamentoSedacionId = c.Id,
                    Valor = decimal.Parse(c.Valor)
                }).ToList();
            }

            if (!string.IsNullOrWhiteSpace(model.LaboratorioJSON))
            {
                var lab = JsonConvert.DeserializeObject<List<SNSListasDTO>>(model.LaboratorioJSON);

                item.SNClaboratorioSNC = lab.Select(c => new SNClaboratorioSNC()
                {
                    LaboratorioSNCid = c.Id,
                    Fecha = DateTime.Parse(c.Fecha),
                    Valor = decimal.Parse(c.Valor)
                }).ToList();
            }

            if (!string.IsNullOrWhiteSpace(model.ImagenesJSON))
            {
                var img = JsonConvert.DeserializeObject<List<SNSListasDTO>>(model.ImagenesJSON);

                item.SNCimagenesSNC = img.Select(c => new SNCimagenesSNC()
                {
                    ImagenesSNCid = c.Id,
                    Fecha = DateTime.Parse(c.Fecha),
                    Valor = decimal.Parse(c.Valor)
                }).ToList();
            }
          
            if (!string.IsNullOrWhiteSpace(model.SxAbstinenciaJSON))
            {
                var sx_abs = JsonConvert.DeserializeObject<List<SNSListasDTO>>(model.SxAbstinenciaJSON);

                item.SNCsxAbstinenciaSNC = sx_abs.Select(c => new SNCsxAbstinenciaSNC()
                {
                    SxAbstinenciaSNCid = c.Id,
                    Valor = decimal.Parse(c.Valor)
                }).ToList();
            }

            if (!string.IsNullOrWhiteSpace(model.SxAbstinenciaMedicacionJSON))
            {
                var sx_abs_medicacion = JsonConvert.DeserializeObject<List<SNSListasDTO>>(model.SxAbstinenciaMedicacionJSON);

                item.SNCmedicacionSNC = sx_abs_medicacion.Select(c => new SNCmedicacionSNC()
                {
                    MedicacionSNCid = c.Id,
                    Valor = decimal.Parse(c.Valor)
                }).ToList();
            }

            if (!string.IsNullOrWhiteSpace(model.ConocidoConvulsionadorJSON))
            {
                var conv = JsonConvert.DeserializeObject<List<SNSListasDTO>>(model.ConocidoConvulsionadorJSON);

                item.SNCanticonvulsionante = conv.Select(c => new SNCanticonvulsionante()
                {
                    AnticonvulsionanteId = c.Id,
                    Valor = decimal.Parse(c.Valor)
                }).ToList();
            }

            _dbContext.SNC.Add(item);

            await _dbContext.SaveChangesAsync();
        }
        #endregion
    }
}