﻿using HistorialClinico.Domain;
using HistorialClinico.Domain.DTO;
using HistorialClinico.Infrastructure;
using HistorialClinico.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace HistorialClinico.Services
{
    public class CamaService : NpgSqlHelperService, ICamaService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly string _connectionString;

        public CamaService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _connectionString = _dbContext.Database.GetDbConnection().ConnectionString;
        }

        public async Task MovimientoCamaPacienteAsync(string TipoMovimientoId, int CamaId, int PacienteId, string user_name)
        {
            var now = DateTime.Now;

            if (TipoMovimientoId == "EGR")
            {
                var item_delelete = await _dbContext.CamaPaciente.FindAsync(CamaId, PacienteId);
                _dbContext.CamaPaciente.Remove(item_delelete);
            }

            if (TipoMovimientoId == "ING")
            {
                var item = new CamaPaciente()
                {
                    CamaId = CamaId,
                    PacienteId = PacienteId,
                    FechaIngreso = now
                };
                _dbContext.CamaPaciente.Add(item);
            }

            var item_mov = new MovimientoCamaPaciente()
            {
                CamaId = CamaId,
                PacienteId = PacienteId,
                TipoMovimientoId = TipoMovimientoId,
                Fecha = now,
                UserName = user_name
            };

            _dbContext.MovimientoCamaPaciente.Add(item_mov);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<CamaPacienteDTO>> GetCamasPacientesAsync()
        {
            var camas = await ExecuteReaderToListAsync<CamaPacienteDTO>("select c.\"Id\", c.\"Nombre\" as \"Cama\", p.\"Id\" as \"PacienteId\", (p.\"Nombres\" || ' ' || p.\"Apellidos\") as \"NombrePaciente\", " +
                "p.\"FechaNacimiento\",cp.\"FechaIngreso\" from \"Cama\" c left join \"CamaPaciente\" cp on c.\"Id\" = cp.\"CamaId\" left join \"Paciente\" p on cp.\"PacienteId\" = p.\"Id\" order by \"Id\"",
                _connectionString,
                CommandType.Text);

            return camas;
        }
    }
}