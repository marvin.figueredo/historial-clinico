﻿using HistorialClinico.Domain.DTO;
using HistorialClinico.Domain;
using HistorialClinico.Infrastructure;
using HistorialClinico.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using Npgsql;
using System.Globalization;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace HistorialClinico.Services
{
    public class HMNService : SqlHelperService, IHMNService
    {
        #region Inicio
        private readonly ApplicationDbContext _dbContext;
        private readonly string _connectionString;
        private readonly CultureInfo ci;

        public HMNService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _connectionString = _dbContext.Database.GetDbConnection().ConnectionString;
            ci = CultureInfo.GetCultureInfo("es-ES");
        }
        #endregion

        #region Metodos publicos
        public async Task<List<GeneralHMN>> ListGeneralHMNAsync()
        {
            var items = await _dbContext.GeneralHMN.ToListAsync();

            return items;
        }

        public async Task<List<BalanceHidricoHMN>> ListBalanceHidricoHMNAsync()
        {
            var items = await _dbContext.BalanceHidricoHMN.ToListAsync();

            return items;
        }

        public async Task<List<LaboratorioHMNDTO>> ListLaboratorioHMNAsync()
        {
            var items = await _dbContext.LaboratorioHMN
                .Select(c => new LaboratorioHMNDTO()
                {
                    Id = c.Id,
                    Nombre = c.Nombre,
                    CategoriaId = c.CategoriaId,
                    Categoria = c.Categoria.Nombre
                }).ToListAsync();

            return items;
        }

        public async Task<List<HMN>> ListHMNAsync(int PacienteId)
        {
            var items = await _dbContext.HMN
                .Where(c => c.PacienteId == PacienteId && !c.Deleted)
                .OrderByDescending(c => c.Id)
                .ToListAsync();

            return items;
        }

        public async Task<List<ListHMNDTO>> HMNGeneralAsync(int HMNId)
        {
            var items = await _dbContext.HMNgeneralHMN.Where(c => c.HMNid == HMNId)
                .Select(c => new ListHMNDTO()
                {
                    Id = c.GeneralHMNid,
                    Nombre = c.GeneralHMN.Nombre,
                    Valor = (c.Valor ?? 0),
                    Formulacion = c.Formulacion,
                    Categoria = ""
                }).ToListAsync();

            return items;
        }

        public async Task<List<ListHMNDTO>> HMNBalanceHidricoHMNlAsync(int HMNId)
        {
            var items = await _dbContext.HMNbalanceHidricoHMN.Where(c => c.HMNid == HMNId)
                .Select(c => new ListHMNDTO()
                {
                    Id = c.BalanceHidricoHMNid,
                    Nombre = c.BalanceHidricoHMN.Nombre,
                    Valor = (c.Valor ?? 0),
                    Formulacion = "",
                    Categoria = ""
                }).ToListAsync();

            return items;
        }

        public async Task<List<ListHMNDTO>> HMNLaboratorioHMNAsync(int HMNId)
        {
            var items = await _dbContext.HMNlaboratorioHMN.Where(c => c.HMNid == HMNId)
                .Select(c => new ListHMNDTO()
                {
                    Id = c.LaboratorioHMNid,
                    Nombre = c.LaboratorioHMN.Nombre,
                    Valor = (c.Valor ?? 0),
                    Formulacion = "",
                    Categoria = c.LaboratorioHMN.Categoria.Nombre
                }).ToListAsync();

            return items;
        }

        public async Task AddHMNAsync(HMNDTO model)
        {
            var item = new HMN()
            {
                PacienteId = model.PacienteId,
               DialisisPeritoneal = model.DialisisPeritoneal,
               FormulacionDialisisPeritoneal = model.FormulacionDialisisPeritoneal,
                Eventos = model.Eventos,
                Planes = model.Planes,
                UserAdd = model.UserName,
                DateAdd = DateTime.Now,
                Deleted = false
            };

            if (!string.IsNullOrWhiteSpace(model.GeneralJSON))
            {
                var gral = JsonConvert.DeserializeObject<List<HMNListasDTO>>(model.GeneralJSON);

                item.HMNgeneralHMN = gral.Select(c => new HMNGeneralHMN()
                {
                    GeneralHMNid = c.Id,
                    Valor = !string.IsNullOrWhiteSpace(c.Valor) ? decimal.Parse(c.Valor) : 0,
                    Formulacion = c.Formulacion
                }).ToList();
            }

            if (!string.IsNullOrWhiteSpace(model.BalanceHidricoJSON))
            {
                var balance = JsonConvert.DeserializeObject<List<HMNListasDTO>>(model.BalanceHidricoJSON);

                item.HMNbalanceHidricoHMN = balance.Select(c => new HMNBalanceHidricoHMN()
                {
                    BalanceHidricoHMNid = c.Id,
                    Valor = !string.IsNullOrWhiteSpace(c.Valor) ? decimal.Parse(c.Valor) : 0
                }).ToList();
            }

            if (!string.IsNullOrWhiteSpace(model.LaboratorioJSON))
            {
                var lab = JsonConvert.DeserializeObject<List<HMNListasDTO>>(model.LaboratorioJSON);

                item.HMNlaboratorioHMN = lab.Select(c => new HMNLaboratorioHMN()
                {
                    LaboratorioHMNid = c.Id,
                    Valor = !string.IsNullOrWhiteSpace(c.Valor) ? decimal.Parse(c.Valor) : 0
                }).ToList();
            }

            _dbContext.HMN.Add(item);

            await _dbContext.SaveChangesAsync();
        }

        #endregion

    }
}