﻿using HistorialClinico.Domain;
using HistorialClinico.Domain.DTO;
using HistorialClinico.Infrastructure;
using HistorialClinico.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using Npgsql;
using System.Globalization;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace HistorialClinico.Services
{
    public class HematologicoService : SqlHelperService, IHematologicoService
    {
        #region Inicio
        private readonly ApplicationDbContext _dbContext;
        private readonly string _connectionString;
        private readonly CultureInfo ci;

        public HematologicoService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _connectionString = _dbContext.Database.GetDbConnection().ConnectionString;
            ci = CultureInfo.GetCultureInfo("es-ES");
        }
        #endregion

        #region Metodos publicos

        public async Task<List<Hematologico>> ListHematologicoAsync(int PacienteId)
        {
            return await _dbContext.Hematologico
                .Where(c => c.PacienteId == PacienteId && !c.Deleted)
                .ToListAsync();
        }

        public async Task AddHematologicoAsync(HematologicoDTO model)
        {
            var item = new Hematologico()
            {
                PacienteId = model.PacienteId,
                HemogramaPai = model.Hemograma_PAI,
                HemogramaHb = model.Hemograma_HB,
                HemogramaHtc = model.Hemograma_HTC,
                HemogramaPlt = model.Hemograma_PLT,
                CrasisTp = model.Crasis_TP,
                CrasisTtpa = model.Crasis_TTPA,
                CrasisFibrinoginos = model.Crasis_Fibrinoginos,
                VitaminaK = model.VitaminaK,
                DosisVitaminaK = model.DosisVitaminaK,
                FechaVitaminaK = model.FechaVitaminaK,
                SangradoActivo = model.SangradoActivo,
                LugarSangrado = model.LugarSangrado,
                Transfusiones = model.Transfusiones,
                TransfusionesGrc = model.Transfusiones_GRC,
                TransfusionesPfc = model.Transfusiones_PFC,
                TransfusionesCrio = model.Transfusiones_CRIO,
                TransfusionesPlt = model.Transfusiones_PLT,
                Eventos = model.Eventos,
                Planes = model.Planes,
                UserAdd = model.UserName,
                DateAdd = DateTime.Now,
                Deleted = false
            };

            _dbContext.Hematologico.Add(item);

            await _dbContext.SaveChangesAsync();
        }
     
        #endregion
    }
}