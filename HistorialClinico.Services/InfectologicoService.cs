﻿using HistorialClinico.Domain.DTO;
using HistorialClinico.Domain;
using HistorialClinico.Infrastructure;
using HistorialClinico.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using Npgsql;
using System.Globalization;
using System.Threading.Tasks;
using System.Linq;

namespace HistorialClinico.Services
{
    public class InfectologicoService : SqlHelperService, IInfectologicoService
    {
        #region Inicio
        private readonly ApplicationDbContext _dbContext;
        private readonly string _connectionString;
        private readonly CultureInfo ci;

        public InfectologicoService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _connectionString = _dbContext.Database.GetDbConnection().ConnectionString;
            ci = CultureInfo.GetCultureInfo("es-ES");
        }
        #endregion

        #region Métodos públicos

        public async Task<List<EstadoInfectologico>> ListEstadoInfectologicoAsync()
        {
            var items = await _dbContext.EstadoInfectologico.ToListAsync();

            return items;
        }

        public async Task<List<Cultivo>> ListCultivoAsync()
        {
            var items = await _dbContext.Cultivo.ToListAsync();

            return items;
        }

        public async Task<List<Hisopado>> ListHisopadoAsync()
        {
            var items = await _dbContext.Hisopado.ToListAsync();

            return items;
        }

        public async Task<List<InfectologicoDTO>> ListInfectologicoAsync(int PacienteId)
        {
            var items = await _dbContext.Infectologico.Where(c => c.PacienteId == PacienteId && !c.Deleted)
                .OrderByDescending(c => c.Id)
                .Select(c => new InfectologicoDTO()
                {
                    Id = c.Id,
                    PacienteId = c.PacienteId,
                    EstadoInfectologicoId = c.EstadoInfectologicoId,
                    EstadoInfectologico = c.EstadoInfectologico.Nombre,
                    Interconsulta = c.Interconsulta,
                    Eventos = c.Eventos,
                    Planes = c.Planes,
                    UserName = c.UserAdd,
                    DateAdd = c.DateAdd
                }).ToListAsync();

            return items;
        }

        public async Task<List<InfectologicoCoberturaAtbDTO>> InfectologicoCoberturaAtbAsync(int InfectologicoId)
        {
            var items = await _dbContext.CoberturaAntibioticos.Where(c => c.InfectologicoId == InfectologicoId)
                .Select(c => new InfectologicoCoberturaAtbDTO()
                {
                    Id = c.Id,
                    Antibiotico = c.Antibiotico,
                    Dosis = c.Dosis,
                    Unidad = c.Unidad,
                    AjustadoClearence = c.AjustadoClearence,
                    FechaInicio = c.FechaInicio,
                    FechaSuspension = c.FechaSuspension
                }).ToListAsync();

            return items;
        }

        public async Task<List<InfectologicoCultivoDTO>> InfectologicoCultivoAsync(int InfectologicoId)
        {
            var items = await _dbContext.CultivoInfectologico.Where(c => c.InfectologicoId == InfectologicoId)
               .Select(c => new InfectologicoCultivoDTO()
               {
                   CultivoId = c.CultivoId,
                   Cultivo = c.Cultivo.Nombre,
                   Resultado = c.Resultado,
                   Fecha = c.Fecha
               }).ToListAsync();

            return items;
        }

        public async Task<List<SensibilidadCultivoInfectologico>> SensibilidadCultivoInfectologicoAsync(int InfectologicoId, int CultivoId)
        {
            var items = await _dbContext.SensibilidadCultivoInfectologico.Where(c => c.InfectologicoId == InfectologicoId && c.CultivoId == CultivoId)
                .ToListAsync();

            return items;
        }

        public async Task<List<HisopadoInfectologicoDTO>> HisopadoInfectologicoAsync(int InfectologicoId)
        {
            var items = await _dbContext.HisopadoInfectologico.Where(c => c.InfectologicoId == InfectologicoId)
               .Select(c => new HisopadoInfectologicoDTO()
               {
                   InfectologicoId = c.InfectologicoId,
                   HisopadoId = c.HisopadoId,
                   Hisopado = c.Hisopado.Nombre,
                   Resultado = c.Resultado,
                   Fecha = c.Fecha
               }).ToListAsync();

            return items;
        }

        public async Task AddInfectologicoAsync(InfectologicoDTO model)
        {
            var item = new Infectologico()
            {
                PacienteId = model.PacienteId,
                EstadoInfectologicoId = model.EstadoInfectologicoId,
                Interconsulta = model.Interconsulta,
                Eventos = model.Eventos,
                Planes = model.Planes,
                UserAdd = model.UserName,
                DateAdd = DateTime.Now,
                Deleted = false
            };

            if (!string.IsNullOrWhiteSpace(model.CoberturaAtbParamJSON))
            {
                var param_atb = JsonConvert.DeserializeObject<List<CoberturaAtbDTO>>(model.CoberturaAtbParamJSON);

                foreach (var atb in param_atb)
                {
                    var new_item = new CoberturaAntibioticos()
                    {
                        Antibiotico = atb.Antibiotico,
                        Dosis = decimal.Parse(atb.Dosis),
                        Unidad = atb.Unidad,
                        AjustadoClearence = atb.AjustadoClearence,
                        FechaInicio = DateTime.Parse(atb.FechaInicio)
                    };

                    if (!string.IsNullOrWhiteSpace(atb.FechaSuspension))
                    {
                        new_item.FechaSuspension = DateTime.Parse(atb.FechaSuspension);
                    }

                    item.CoberturaAntibioticos.Add(new_item);
                }
            }

            if (!string.IsNullOrWhiteSpace(model.CultivoParamJSON))
            {
                var param_cultivo = JsonConvert.DeserializeObject<List<CultivoDTO>>(model.CultivoParamJSON);

                item.CultivoInfectologico = param_cultivo.Select(c => new CultivoInfectologico()
                {
                    CultivoId = c.CultivoId,
                    Resultado = c.Resultado,
                    Fecha = DateTime.Parse(c.Fecha)
                }).ToList();
            }

            if (!string.IsNullOrWhiteSpace(model.SensibilidadParamJSON))
            {
                var param_sensibilidad = JsonConvert.DeserializeObject<List<SensibilidadDTO>>(model.SensibilidadParamJSON);

                item.SensibilidadCultivoInfectologico = param_sensibilidad.Select(c => new SensibilidadCultivoInfectologico()
                {
                    CultivoId = c.CultivoId,
                    Sensibilidad = c.Sensibilidad
                }).ToList();
            }

            if (!string.IsNullOrWhiteSpace(model.HisopadoParamJSON))
            {
                var param_hisopado = JsonConvert.DeserializeObject<List<HisopadoDTO>>(model.HisopadoParamJSON);

                item.HisopadoInfectologico = param_hisopado.Select(c => new HisopadoInfectologico()
                {
                    HisopadoId = c.HisopadoId,
                    Resultado = c.Resultado,
                    Fecha = DateTime.Parse(c.Fecha)
                }).ToList();
            }

            _dbContext.Infectologico.Add(item);

            await _dbContext.SaveChangesAsync();
        }

        #endregion
    }
}