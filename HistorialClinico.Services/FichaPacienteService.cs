﻿using AutoMapper;
using HistorialClinico.Domain;
using HistorialClinico.Domain.DTO;
using HistorialClinico.Infrastructure;
using HistorialClinico.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace HistorialClinico.Services
{
    public class FichaPacienteService: NpgSqlHelperService, IFichaPacienteService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IMapper mapper;
        private readonly string _connectionString;

        public FichaPacienteService(ApplicationDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            this.mapper = mapper;
            _connectionString = _dbContext.Database.GetDbConnection().ConnectionString;
        }

        public async Task<IEnumerable<PacienteDTO>> ListarPacientesAsync(string Valor)
        {
            var item = await _dbContext.Pacientes.Where(c => c.Nombres.ToUpper().Contains(Valor.ToUpper()) || c.Apellidos.ToUpper().Contains(Valor.ToUpper()))
                .ToListAsync();

            return mapper.Map<IEnumerable<PacienteDTO>>(item);
        }

        public async Task<PacienteDTO> AddEditDatosBasicosPacienteAsync(PacienteDTO model)
        {
            var now = DateTime.Now;

            Paciente item;

            if (model.Id > 0)
            {
                item = await _dbContext.Paciente.FindAsync(model.Id);
            }
            else
            {
                item = new Paciente();
            }

            item.NroDocumento = model.NroDocumento;
            item.Nombres = model.Nombres;
            item.Apellidos = model.Apellidos;
            item.Sexo = model.Sexo;
            item.FechaNacimiento = model.FechaNacimiento;
            item.GrupoSanguineo = model.GrupoSanguineo;
            item.Peso = model.Peso;

            if (model.Id > 0)
            {
                _dbContext.Paciente.Update(item);
            }
            else
            {
                _dbContext.Paciente.Add(item);

                if (model.CamaId.HasValue)
                {
                    var cama_paciente = new CamaPaciente()
                    {
                        CamaId = model.CamaId.Value,
                        Paciente = item,
                        FechaIngreso = now
                    };
                    _dbContext.CamaPaciente.Add(cama_paciente);

                    var item_mov = new MovimientoCamaPaciente()
                    {
                        CamaId = model.CamaId.Value,
                        Paciente = item,
                        TipoMovimientoId = "ING",
                        Fecha = now,
                        UserName = model.UserName
                    };

                    _dbContext.MovimientoCamaPaciente.Add(item_mov);
                }
            }

            var hist = new PacienteTablaHistorica()
            {
                Paciente = item,
                NroDocumento = model.NroDocumento,
                Nombres = model.Nombres,
                Apellidos = model.Apellidos,
                Sexo = model.Sexo,
                FechaNacimiento = model.FechaNacimiento,
                GrupoSanguineo = model.GrupoSanguineo,
                Peso = model.Peso,
                FechaAdd = now,
                UserNameAdd = model.UserName
            };
            _dbContext.PacienteTablaHistorica.Add(hist);

            await _dbContext.SaveChangesAsync();

            return mapper.Map<PacienteDTO>(item);
        }

        public async Task AddEditContactoPacienteAsync(IEnumerable<ContactoPacienteDTO> contactos)
        {
            contactos = contactos.Where(c => c.Id > 0 || !string.IsNullOrWhiteSpace(c.TipoContactoId) || !string.IsNullOrWhiteSpace(c.NombreContacto) || !string.IsNullOrWhiteSpace(c.NroContacto));

            if (contactos == null || contactos.Count() == 0)
            {
                return;
            }

            var old_items = _dbContext.ContactoPaciente.Where(c => c.PacienteId == contactos.FirstOrDefault().PacienteId);

            await old_items.ForEachAsync(c => _dbContext.ContactoPaciente.Remove(c));
            
            foreach (var contacto in contactos)
            {
                var new_item = new ContactoPaciente()
                {
                    PacienteId = contacto.PacienteId,
                    NombreContacto = contacto.NombreContacto,
                    NroContacto = contacto.NroContacto,
                    TipoContactoId = contacto.TipoContactoId,
                    FechaAdd = DateTime.Now,
                    UserNameAdd = contacto.UserName
                };
                _dbContext.ContactoPaciente.Add(new_item);
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task AddEditDiagnosticoAsync(DiagnosticoDTO model)
        {
            var now = DateTime.Now;

            Diagnostico item;

            if (model.Id == 0)
            {
                item = new Diagnostico()
                {
                    PacienteId = model.PacienteId,
                    FechaAdd = now,
                    UserNameAdd = model.UserName
                };
            }
            else
            {
                item = await _dbContext.Diagnostico.FindAsync(model.Id);
                item.FechaUpdate = now;
                item.UserNameUpdate = model.UserName;
            }

            item.Resumen = model.Resumen;

            if (model.Id == 0)
            {
                _dbContext.Diagnostico.Add(item);
            }
            else
            {
                _dbContext.Diagnostico.Update(item);
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<PRMS>> ListarPRMS()
        {
            var items = await ExecuteReaderToListAsync<PRMS>("select \"Id\", \"Nombre\" from \"PRMS\"", _connectionString, CommandType.Text);

            return items;
        }

        public async Task EditPRMSAsync(int PacienteId, int PRMSId)
        {
            var item = await _dbContext.Paciente.FindAsync(PacienteId);

            if (PRMSId > 0)
            {
                item.PrmsId = PRMSId;
            }
            else
            {
                item.PrmsId = null;
            }

            _dbContext.Paciente.Update(item);

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteDiagnosticoAsync(int DiagnosticoId, string user_name)
        {
            var item = await _dbContext.Diagnostico.FindAsync(DiagnosticoId);
            item.FechaDelete = DateTime.Now;
            item.UserNameDelete = user_name;
            _dbContext.Diagnostico.Update(item);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<PacienteDTO> GetDatosBasicosByIdAsync(int Id)
        {
            var item = await _dbContext.Paciente.FindAsync(Id);

            return mapper.Map<PacienteDTO>(item);
        }

        public async Task<List<ContactoPacienteDTO>> GetContactosPacienteAsync(int PacienteId)
        {
            var contactos = await _dbContext.ContactoPaciente.Where(c => c.PacienteId == PacienteId && !c.FechaDelete.HasValue)
                .ToListAsync();

            return mapper.Map<List<ContactoPacienteDTO>>(contactos);
        }

        public async Task<List<DiagnosticoDTO>> GetDiagnosticosAsync(int PacienteId)
        {
            var items = await _dbContext.Diagnostico.Where(c => c.PacienteId == PacienteId && !c.FechaDelete.HasValue)
                .OrderByDescending(c => c.FechaAdd)
                .ToListAsync();

            return mapper.Map<List<DiagnosticoDTO>>(items);
        }
    }
}