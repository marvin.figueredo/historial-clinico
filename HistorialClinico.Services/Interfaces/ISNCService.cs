﻿using HistorialClinico.Domain;
using HistorialClinico.Domain.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HistorialClinico.Services.Interfaces
{
    public interface ISNCService
    {
        Task<List<AspectoGeneralSNC>> ListAspectoGralAsync();
        Task<List<Sedacion>> ListSedacionAsync();
        Task<List<MedicamentoSedacion>> ListMedicamentoSedacionAsync();
        Task<List<LaboratorioSNC>> ListLaboratorioSNCAsync();
        Task<List<ImagenSNC>> ListImagenSNCAsync();
        Task<List<SxAbstinenciaSNC>> ListSxAbstinenciaSNCAsync();
        Task<List<MedicacionSNC>> ListMedicacionSNCAsync();
        Task<List<Anticonvulsionante>> ListAnticonvulsionanteAsync();
        Task AddSNCAsync(SNCDTO model);
        Task<List<SNCDTO>> ListSNCAsync(int PacienteId);
        Task<List<ListSNCDTO>> SNCAspectoGralAsync(int SNCId);
        Task<List<ListSNCDTO>> SNCMedicamentoSedacionAsync(int SNCId);
        Task<List<ListSNCDTO>> SNCLaboratorioAsync(int SNCId);
        Task<List<ListSNCDTO>> SNCImagenesAsync(int SNCId);
        Task<List<ListSNCDTO>> SNCAnticonvulsionanteAsync(int SNCId);
        Task<List<ListSNCDTO>> SNCSxAbstinenciaAsync(int SNCId);
        Task<List<ListSNCDTO>> SNCAbstinenciaMedicacionAsync(int SNCId);
    }
}
