﻿using HistorialClinico.Domain;
using HistorialClinico.Domain.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HistorialClinico.Services.Interfaces
{
    public interface IHMNService
    {
        Task<List<GeneralHMN>> ListGeneralHMNAsync();
        Task<List<BalanceHidricoHMN>> ListBalanceHidricoHMNAsync();
        Task<List<LaboratorioHMNDTO>> ListLaboratorioHMNAsync();
        Task AddHMNAsync(HMNDTO model);
        Task<List<HMN>> ListHMNAsync(int PacienteId);
        Task<List<ListHMNDTO>> HMNGeneralAsync(int HMNId);
        Task<List<ListHMNDTO>> HMNBalanceHidricoHMNlAsync(int HMNId);
        Task<List<ListHMNDTO>> HMNLaboratorioHMNAsync(int HMNId);
    }
}
