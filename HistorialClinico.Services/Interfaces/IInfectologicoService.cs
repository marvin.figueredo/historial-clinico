﻿using HistorialClinico.Domain.DTO;
using HistorialClinico.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HistorialClinico.Services.Interfaces
{
    public interface IInfectologicoService
    {
        Task<List<InfectologicoDTO>> ListInfectologicoAsync(int PacienteId);
        Task<List<InfectologicoCoberturaAtbDTO>> InfectologicoCoberturaAtbAsync(int InfectologicoId);
        Task<List<InfectologicoCultivoDTO>> InfectologicoCultivoAsync(int InfectologicoId);
        Task<List<SensibilidadCultivoInfectologico>> SensibilidadCultivoInfectologicoAsync(int InfectologicoId, int CultivoId);
        Task<List<HisopadoInfectologicoDTO>> HisopadoInfectologicoAsync(int InfectologicoId);
        Task<List<EstadoInfectologico>> ListEstadoInfectologicoAsync();
        Task<List<Cultivo>> ListCultivoAsync();
        Task<List<Hisopado>> ListHisopadoAsync();
        Task AddInfectologicoAsync(InfectologicoDTO model);
    }
}