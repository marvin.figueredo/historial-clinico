﻿using HistorialClinico.Domain.DTO;
using HistorialClinico.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HistorialClinico.Services.Interfaces
{
    public interface IApRespiratorioService
    {
        Task<List<ApRespiratorioDTO>> ListApRespiratorioAsync(int PacienteId);
        Task<List<SoporteRespiratorio>> ListSoporteRespiratorioAsync();
        Task<List<SoporteRespParametrosDTO>> ListSoporteRespParametrosAsync(int ApRespiratorioId);
        Task<List<Ventilacion>> ListVentilacionAsync();
        Task<List<Modalidad>> ListModalidadAsync();
        Task<List<Gasometria>> ListGasometriaAsync();
        Task<List<GasometriaParametrosDTO>> ListGasometriaParametrosAsync(int ApRespiratorioId);
        Task AddApRespiratorioAsync(ApRespiratorioDTO model);
    }
}