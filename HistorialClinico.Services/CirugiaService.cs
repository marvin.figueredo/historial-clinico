﻿using HistorialClinico.Domain;
using HistorialClinico.Domain.DTO;
using HistorialClinico.Infrastructure;
using HistorialClinico.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace HistorialClinico.Services
{
    public class CirugiaService : ICirugiaService
    {
        #region Inicio
        private readonly ApplicationDbContext _dbContext;

        public CirugiaService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        #endregion

        #region Metodos publicos

        public async Task AddCirugiaAsync(CirugiaDTO model)
        {
            var cirugia = new Cirugia()
            {
                PacienteId = model.PacienteId,
                TuvoCirugia = model.TuvoCirugia,
                Tecnica = model.Tecnica,
                Hallazgos = model.Hallazgos,
                Procedimiento = model.Procedimiento,
                CualProcedimiento = model.CualProcedimiento,
                OtrasAcotaciones = model.OtrasAcotaciones,
                UserAdd = model.UserAdd,
                DateAdd = DateTime.Now,
                Deleted = false
            };

            _dbContext.Cirugia.Add(cirugia);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Cirugia>> ListCirugiaAsync(int PacienteId)
        {
            var items = await _dbContext.Cirugia.Where(c => c.PacienteId == PacienteId)
                .OrderByDescending(c => c.DateAdd)
                .ToListAsync();

            return items;
        }
     
        #endregion
    }
}