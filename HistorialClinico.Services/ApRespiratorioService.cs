﻿using HistorialClinico.Domain.DTO;
using HistorialClinico.Domain;
using HistorialClinico.Infrastructure;
using HistorialClinico.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using Npgsql;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace HistorialClinico.Services
{
    public class ApRespiratorioService : SqlHelperService, IApRespiratorioService
    {
        #region Inicio
        private readonly ApplicationDbContext _dbContext;
        private readonly string _connectionString;

        public ApRespiratorioService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _connectionString = _dbContext.Database.GetDbConnection().ConnectionString;
        }
        #endregion

        #region Métodos públicos

        public async Task<List<ApRespiratorioDTO>> ListApRespiratorioAsync(int PacienteId)
        {
            var items = await _dbContext.ApRespiratorio.Where(c => c.PacienteId == PacienteId && !c.Deleted)
                .OrderByDescending(c => c.Id)
                .Select(c => new ApRespiratorioDTO()
                {
                    Id = c.Id,
                    PacienteId = c.PacienteId,
                    SoporteRespiratorioId = c.SoporteRespiratorioId,
                    SoporteRespiratorio = !c.SoporteRespiratorioId.HasValue ? "" : c.SoporteRespiratorio.Nombre,
                    ValorSoporteResp = c.ValorSoporteResp,
                    Parametros = !c.SoporteRespiratorioId.HasValue ? false : c.SoporteRespiratorio.Parametros,
                    Ventilacion = !c.VentilacionId.HasValue ? "" : c.Ventilacion.Nombre,
                    VentilacionId = c.VentilacionId,
                    ModalidadId = c.ModalidadId,
                    Modalidad = !c.ModalidadId.HasValue ? "" : c.Modalidad.Nombre,
                    GasometriaId = c.GasometriaId,
                    Gasometria = !c.GasometriaId.HasValue ? "" : c.Gasometria.Nombre,
                    Manejo = c.Manejo,
                    Eventos = c.Eventos,
                    Planes = c.Planes,
                    UserName = c.UserAdd,
                    DateAdd = c.DateAdd
                }).ToListAsync();

            return items;
        }

        public async Task<List<SoporteRespiratorio>> ListSoporteRespiratorioAsync()
        {
            var items = await _dbContext.SoporteRespiratorio.ToListAsync();

            return items;
        }
        
        public async Task<List<Ventilacion>> ListVentilacionAsync()
        {
            var items = await _dbContext.Ventilacion.ToListAsync();

            return items;
        }

        public async Task<List<Modalidad>> ListModalidadAsync()
        {
            var items = await _dbContext.Modalidad.ToListAsync();

            return items;
        }

        public async Task<List<Gasometria>> ListGasometriaAsync()
        {
            var items = await _dbContext.Gasometria.ToListAsync();

            return items;
        }

        public async Task<List<SoporteRespParametrosDTO>> ListSoporteRespParametrosAsync(int ApRespiratorioId)
        {
            List<SoporteRespParametrosDTO> model = new List<SoporteRespParametrosDTO>();

            var items = await _dbContext.SoporteRespiratorio.ToListAsync();

            foreach (var item in items)
            {
                var item_asociado = await _dbContext.ApRespiratorioParametros.FindAsync(ApRespiratorioId, item.Id);

                model.Add(new SoporteRespParametrosDTO()
                {
                    Id = item.Id,
                    Nombre = item.Nombre,
                    Valor = item_asociado == null ? 0 : item_asociado.Valor
                });
            }

            return model;
        }

        public async Task<List<GasometriaParametrosDTO>> ListGasometriaParametrosAsync(int ApRespiratorioId)
        {
            List<GasometriaParametrosDTO> model = new List<GasometriaParametrosDTO>();

            var items = await _dbContext.ParametrosGasometria.ToListAsync();

            foreach (var item in items)
            {
                var item_asociado = await _dbContext.ApRespiratorioParametrosGasometria.FindAsync(ApRespiratorioId, item.Id);

                model.Add(new GasometriaParametrosDTO()
                {
                    Id = item.Id,
                    Nombre = item.Nombre,
                    Valor = item_asociado == null ? 0 : item_asociado.Valor
                });
            }

            return model;
        }

        public async Task AddApRespiratorioAsync(ApRespiratorioDTO model)
        {

            var item = new ApRespiratorio()
            {
                PacienteId = model.PacienteId,
                SoporteRespiratorioId = model.SoporteRespiratorioId,
                ValorSoporteResp = model.ValorSoporteResp,
                VentilacionId = model.VentilacionId,
                ModalidadId = model.ModalidadId,
                GasometriaId = model.GasometriaId,
                Manejo = model.Manejo,
                Eventos = model.Eventos,
                Planes = model.Planes,
                UserAdd = model.UserName,
                DateAdd = DateTime.Now,
                Deleted = false
            };

            if (!string.IsNullOrWhiteSpace(model.SoporteRespiratorioParamJSON))
            {
                var param_soporte = JsonConvert.DeserializeObject<List<ParametroDTO>>(model.SoporteRespiratorioParamJSON);

                item.ApRespiratorioParametros = param_soporte.Select(c => new ApRespiratorioParametros()
                {
                    ParametroId = c.Id,
                    Valor = decimal.Parse(c.Valor)
                }).ToList();
            }

            if (!string.IsNullOrWhiteSpace(model.GasometriaParamJSON))
            {
                var param_gasom = JsonConvert.DeserializeObject<List<ParametroDTO>>(model.GasometriaParamJSON);

                item.ApRespiratorioParametrosGasometria = param_gasom.Select(c => new ApRespiratorioParametrosGasometria()
                {
                    ParametroId = c.Id,
                    Valor = decimal.Parse(c.Valor)
                }).ToList();
            }

            _dbContext.ApRespiratorio.Add(item);

            await _dbContext.SaveChangesAsync();
        }

        #endregion
    }
}